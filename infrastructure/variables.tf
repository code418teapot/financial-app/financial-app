variable "access_token" {
  type = string
}

variable "cloud_id" {
  type = string
}

variable "folder_id" {
  type = string
}

variable "availability_zone" {
  type = string
  default = "ru-central1-a"
}

variable "environment" {
  type = string
  default = "dev"
}

variable "prefix" {
  type = string
  default = "finapp"
}

variable "gitlab_token_name" {
  type = string
}

variable "gitlab_token_password" {
  type = string
}