# Update your existing list of packages
echo "Package repository configuration started..."
sudo apt update -y

# Install prerequisite packages
sudo apt install -y apt-transport-https ca-certificates curl software-properties-common

# Add GPG key of the official Docker repo
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Add Docker to APT sources
sudo add-apt-repository -y "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"

# Make sure to install Docker from the docker repo
apt-cache policy docker-ce
echo "Package repository configuration completed"

# Install Docker
echo "Docker installation started..."
sudo apt install -y docker-ce
echo "Docker installation completed"

# User setup
echo "User setup started..."
sudo usermod -aG docker ${USER}
echo "User setup completed"
