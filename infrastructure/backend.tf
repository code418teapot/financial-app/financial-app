// OS Image to use
resource "yandex_compute_image" "ubuntu_2004" {
  source_family = "ubuntu-2004-lts"
}

// Define cloud network
resource "yandex_vpc_network" "vpc_network" {
  name = "${var.prefix}-network-${var.environment}"
}

// Define cloud subnetwork in ru-central1a zone
resource "yandex_vpc_subnet" "vpc_subnet" {
  name           = "${var.prefix}-subnet1-${var.environment}"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.vpc_network.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}

// Define virtual machine instance
resource "yandex_compute_instance" "backend" {
  name = "${var.prefix}-backend-${var.environment}"

  resources {
    cores  = 2
    memory = 4
  }

  boot_disk {
    initialize_params {
      image_id = yandex_compute_image.ubuntu_2004.id
      type = "network-ssd"
      size = 30  
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.vpc_subnet.id
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("keys/id_finance_app.pub")}"
  }

  // Define SSH connection to the instance
  connection {
    type            = "ssh"
    user            = "ubuntu"
    private_key     = "${file("keys/id_finance_app")}"
    host            = "${self.network_interface.0.nat_ip_address}"
  }

  // Copy setup script
  provisioner "file" {
    source      = "scripts/setup.sh"
    destination = "/tmp/setup.sh"
  }

  // Copy setup script
  provisioner "file" {
    source      = "scripts/setup-compose.sh"
    destination = "/tmp/setup-compose.sh"
  }

  // Run setup script
  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/setup.sh",
      "/tmp/setup.sh",
    ]
  }

  // Run setup-compose script
  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/setup-compose.sh",
      "/tmp/setup-compose.sh",
    ]
  }

  // Run setup-compose script
  provisioner "remote-exec" {
    inline = [
      "docker login registry.gitlab.com -u ${var.gitlab_token_name} -p ${var.gitlab_token_password}"
    ]
  }
}

output "internal_ip_address" {
  value = yandex_compute_instance.backend.network_interface.0.ip_address
}

output "external_ip_address" {
  value = yandex_compute_instance.backend.network_interface.0.nat_ip_address
}

output "subnet" {
  value = yandex_vpc_subnet.vpc_subnet.id
}