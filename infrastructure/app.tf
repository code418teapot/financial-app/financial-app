// Create service account
resource "yandex_iam_service_account" "service_account" {
  folder_id = var.folder_id
  name      = "${var.prefix}-${var.environment}-sa"
}

// Grant permissions to access Object Storage
resource "yandex_resourcemanager_folder_iam_member" "service_account_editor" {
  folder_id = var.folder_id
  role      = "storage.editor"
  member    = "serviceAccount:${yandex_iam_service_account.service_account.id}"
}

// Create Static Access Keys
resource "yandex_iam_service_account_static_access_key" "service_account_static_key" {
  service_account_id = yandex_iam_service_account.service_account.id
  description        = "Static access key for object storage"
}

resource "yandex_storage_bucket" "bucket_app" {
  bucket = "${var.prefix}-app-${var.environment}"

  // Enable public access
  acl    = "public-read"

  // Configure access to the bucket
  access_key = yandex_iam_service_account_static_access_key.service_account_static_key.access_key
  secret_key = yandex_iam_service_account_static_access_key.service_account_static_key.secret_key

  // Configure static site hosting
  website {
    index_document = "index.html"
    error_document = "index.html"
  }
}