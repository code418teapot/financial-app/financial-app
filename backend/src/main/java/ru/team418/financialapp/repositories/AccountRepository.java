package ru.team418.financialapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.team418.financialapp.entities.Account;

import java.util.List;
import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

    @Query(value = "select * from public.accounts where user_id = :userId", nativeQuery = true)
    List<Account> findAllByUserId(@Param("userId") Long userId);

    @Query(value = "select * from public.accounts where user_id = :userId and id = :accountId", nativeQuery = true)
    Optional<Account> findAccountByUserIdAndAccountId(@Param("userId") Long userId, @Param("accountId") Long accountId);

    Optional<Account> findAccountById(Long id);
}