package ru.team418.financialapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.team418.financialapp.entities.Unit;

import java.util.Optional;

@Repository
public interface UnitRepository extends JpaRepository<Unit, Long> {

    @Query(value = "select * from public.units where currency = :currency", nativeQuery = true)
    Optional<Unit> findUnitByCurrency(@Param("currency") String currency);
}