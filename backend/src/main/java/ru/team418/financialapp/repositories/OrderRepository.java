package ru.team418.financialapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.team418.financialapp.entities.Order;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    @Query(value = "select distinct o.* from public.orders o " +
            "join public.accounts acc on (acc.id = o.account_sender_id or acc.id = o.account_receiver_id) " +
            "where acc.user_id = :userId order by o.created_date desc", nativeQuery = true)
    List<Order> findAllByUserId(Long userId);
}