package ru.team418.financialapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.team418.financialapp.entities.RefreshToken;

import java.util.Optional;
import java.util.UUID;

/**
 * Интерфейс для работы с таблицей refresh-токенов в БД
 */
@Repository
public interface RefreshTokenRepository extends JpaRepository<RefreshToken, Long> {

    Optional<RefreshToken> findByToken(UUID token);
}
