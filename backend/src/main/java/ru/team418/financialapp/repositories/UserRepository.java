package ru.team418.financialapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.team418.financialapp.dto.UserDto;
import ru.team418.financialapp.entities.User;

import java.util.List;
import java.util.Optional;

/**
 * Интерфейс для работы с таблицей пользователей в БД
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Override
    Optional<User> findById(Long id);

    Optional<User> findByEmail(String email);

    @Modifying
    @Query("update User u set u.active = :isActive where u.id = :id")
    void setActiveUser(@Param("id") Long id, @Param("isActive") boolean isActive);

    @Query("select u.active from User u where u.id = :userId")
    boolean checkConfirmed(@Param("userId") Long userId);

    @Query(value = "select * from public.users u join user_roles r on r.user_id = u.id where not r.role = :roleName", nativeQuery = true)
    List<User> findAllUsersWithoutAdmins(@Param("roleName") String roleName);
}
