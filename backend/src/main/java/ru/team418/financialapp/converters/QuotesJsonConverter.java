package ru.team418.financialapp.converters;

import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.util.CollectionUtils;
import ru.team418.financialapp.dto.quotes.timeseries.TimeSeriesValueDto;
import ru.team418.financialapp.utils.JsonUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Collections;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * Конвертор списка котировок
 */
@Converter(autoApply = true)
public class QuotesJsonConverter implements AttributeConverter<List<TimeSeriesValueDto>, String> {
    @Override
    public String convertToDatabaseColumn(List<TimeSeriesValueDto> quotes) {
        if (CollectionUtils.isEmpty(quotes)) {
            return null;
        }
        return JsonUtils.asJsonString(quotes);
    }

    @Override
    public List<TimeSeriesValueDto> convertToEntityAttribute(String quotes) {
        if (isBlank(quotes)) {
            return Collections.emptyList();
        }
        return JsonUtils.fromJsonString(quotes, new TypeReference<>() {
        });
    }
}