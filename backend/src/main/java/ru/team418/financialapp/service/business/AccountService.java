package ru.team418.financialapp.service.business;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.team418.financialapp.dto.AccountDto;
import ru.team418.financialapp.dto.BalanceOperationWithAccountDto;
import ru.team418.financialapp.dto.BalanceOperationWithAccountsDto;
import ru.team418.financialapp.dto.units.UnitDto;
import ru.team418.financialapp.entities.Account;
import ru.team418.financialapp.entities.User;
import ru.team418.financialapp.exceptions.BalanceOperationException;
import ru.team418.financialapp.repositories.AccountRepository;
import ru.team418.financialapp.service.infrastructure.UserService;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

@Service
@AllArgsConstructor
public class AccountService {

    private static final String IP_OWNERSHIP_FORM_CODE = "408";
    private static final String COMMERCIAL_ORG_CODE = "02";
    private static final String CONTROL_NUMBER = "7";
    private static final String BANK_BRANCH_CODE = "0117";

    private final UserService userService;
    private final AccountRepository accountRepository;
    private final UnitsService unitsService;

    public AccountDto createAccount(Long userId, String currency) {
        Account.Currency currencyEnum = Account.Currency.fromValue(currency);
        if (Account.Currency.fromValue(currency) == null) {
            throw new IllegalArgumentException("Неверный или неподдерживаемый код валюты");
        }

        User user = userService.findUserById(userId);

        long count = accountRepository.count() + 1;
        var account = new Account();
        String accountNumber = IP_OWNERSHIP_FORM_CODE + COMMERCIAL_ORG_CODE + currencyEnum.getAccountCode()
                + CONTROL_NUMBER + BANK_BRANCH_CODE + String.format("%07d", count);
        account.setUser(user);
        account.setCurrency(currencyEnum);
        account.setNumber(accountNumber);
        account.setBalance(BigDecimal.ZERO);
        account.setCreatedDate(LocalDateTime.now());

        Account savedAccount = accountRepository.save(account);
        List<UnitDto> unitDtos = unitsService.getAllUnits_updateOnlyExchangeRate();
        return toDto(savedAccount, unitDtos);
    }


    public List<AccountDto> getAccountsByClientId(Long userId) {
        List<Account> accounts = accountRepository.findAllByUserId(userId);
        List<UnitDto> unitDtos = unitsService.getAllUnits_updateOnlyExchangeRate();


        return accounts.stream()
                .map(account -> toDto(account, unitDtos))
                .toList();
    }

    public Account getAccountById(Long userId, Long accountId) {
        return accountRepository.findAccountByUserIdAndAccountId(userId, accountId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Счет с id %s не найден", accountId)));
    }

    public AccountDto getAccountByIdDto(Long userId, Long accountId) {
        Account account = accountRepository.findAccountByUserIdAndAccountId(userId, accountId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Счет с id %s не найден", accountId)));
        UnitDto unitDto = unitsService.updateUnitExchangeRate(account);

        return toDto(account, Collections.singletonList(unitDto));
    }

    public Account getAccountById(Long accountId) {
        return accountRepository
                .findAccountById(accountId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Счет с id %s не найден", accountId)));
    }

    public AccountDto depositAccount(Long userId, BalanceOperationWithAccountDto dto) {
        Account account = accountRepository.findAccountByUserIdAndAccountId(userId, dto.getAccountId())
                .orElseThrow(() -> new EntityNotFoundException(String.format("Счет с id %s не найден", dto.getAccountId())));

        if (dto.getAmount().compareTo(BigDecimal.ZERO) < 0) {
            throw new BalanceOperationException("Попытка зачисления отрицательной суммы на счет");
        }
        account.addBalance(dto.getAmount());
        UnitDto unitDto = unitsService.updateUnitExchangeRate(account);
        return toDto(accountRepository.save(account), Collections.singletonList(unitDto));
    }

    public AccountDto depositAccount(BalanceOperationWithAccountDto dto) {
        Account account = accountRepository.findAccountById(dto.getAccountId())
                .orElseThrow(() -> new EntityNotFoundException(String.format("Счет с id %s не найден", dto.getAccountId())));

        if (dto.getAmount().compareTo(BigDecimal.ZERO) < 0) {
            throw new BalanceOperationException("Попытка зачисления отрицательной суммы на счет");
        }
        account.addBalance(dto.getAmount());
        UnitDto unitDto = unitsService.updateUnitExchangeRate(account);
        return toDto(accountRepository.save(account), Collections.singletonList(unitDto));
    }

    public AccountDto writeOffMoneyFromAccount(Long userId, BalanceOperationWithAccountDto dto) {
        Account account = accountRepository.findAccountByUserIdAndAccountId(userId, dto.getAccountId())
                .orElseThrow(() -> new EntityNotFoundException(String.format("Счет списания с id %s не найден", dto.getAccountId())));

        if (account.getBalance().subtract(dto.getAmount()).compareTo(BigDecimal.ZERO) < 0) {
            throw new BalanceOperationException("Недостаточно средств на счету");
        }
        account.writeOffBalance(dto.getAmount());
        UnitDto unitDto = unitsService.updateUnitExchangeRate(account);
        return toDto(accountRepository.save(account), Collections.singletonList(unitDto));
    }

    public AccountDto writeOffMoneyFromAccount(BalanceOperationWithAccountDto dto) {
        Account account = accountRepository.findAccountById(dto.getAccountId())
                .orElseThrow(() -> new EntityNotFoundException(String.format("Счет списания с id %s не найден", dto.getAccountId())));

        if (account.getBalance().subtract(dto.getAmount()).compareTo(BigDecimal.ZERO) < 0) {
            throw new BalanceOperationException("Недостаточно средств на счету");
        }
        account.writeOffBalance(dto.getAmount());
        UnitDto unitDto = unitsService.updateUnitExchangeRate(account);
        return toDto(accountRepository.save(account), Collections.singletonList(unitDto));
    }

    @Transactional
    public void transferMoney(Long userId, BalanceOperationWithAccountsDto dto) {
        Account senderAcc = accountRepository.findAccountByUserIdAndAccountId(userId, dto.getSender().getAccountId())
                .orElseThrow(() -> new EntityNotFoundException(String.format("Счет отправителя с id %s не найден", dto.getSender().getAccountId())));
        Account receiverAcc = accountRepository.findAccountByUserIdAndAccountId(userId, dto.getReceiver().getAccountId())
                .orElseThrow(() -> new EntityNotFoundException(String.format("Счет получателя с id %s не найден", dto.getReceiver().getAccountId())));

        if (senderAcc.getCurrency() != receiverAcc.getCurrency()) {
            throw new BalanceOperationException("Переводить деньги возможно только между счетами одинаковой валюты");
        }
        BigDecimal amount = dto.getSender().getAmount();
        senderAcc.writeOffBalance(amount);
        receiverAcc.addBalance(amount);

        unitsService.updateUnitExchangeRate(senderAcc);
        unitsService.updateUnitExchangeRate(receiverAcc);
        accountRepository.save(senderAcc);
        accountRepository.save(receiverAcc);
    }

    @Transactional
    public void closeAccount(Long userId, BalanceOperationWithAccountsDto dto) {
        AccountDto accountDto = this.sellAllUnits(userId, dto.getSender().getAccountId());
        dto.setSender(new BalanceOperationWithAccountDto(accountDto.id(), accountDto.balance()));
        this.transferMoney(userId, dto);

        accountRepository.deleteById(dto.getSender().getAccountId());
    }

    public List<Account> findAll() {
        return accountRepository.findAll();
    }

    public List<Account> saveAll(List<Account> entities) {
        return accountRepository.saveAll(entities);
    }

    public static AccountDto toDto(Account entity, List<UnitDto> unitDtos) {
        BigDecimal rubBalance;
        if (Account.Currency.RUB != entity.getCurrency()) {
            BigDecimal exchangeRate = unitDtos.stream()
                    .filter(unitDto -> entity.getCurrency() == unitDto.getCurrency())
                    .findFirst()
                    .orElseThrow(() -> new EntityNotFoundException(String.format("Данные по валюте %s не найдены", entity.getCurrency().toString())))
                    .getExchangeRate();
            rubBalance = entity.getBalance().multiply(exchangeRate);
        } else {
            rubBalance = entity.getBalance();
        }
        return new AccountDto(entity.getId(), entity.getCreatedDate(), entity.getUpdatedDate(), entity.getCurrency(), entity.getNumber(), entity.getBalance(), rubBalance);
    }

    private AccountDto sellAllUnits(Long userId, Long accountId) {
        /* todo: алгоритм продажи юнитов
        1. получить сумму стоимости юнитов валюты
        2. списать юниты
         */

        return this.depositAccount(userId, new BalanceOperationWithAccountDto(accountId, BigDecimal.ONE));
    }
}
