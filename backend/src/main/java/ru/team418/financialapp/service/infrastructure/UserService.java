package ru.team418.financialapp.service.infrastructure;

import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.team418.financialapp.dto.UserDto;
import ru.team418.financialapp.entities.User;
import ru.team418.financialapp.exceptions.UserExistsException;
import ru.team418.financialapp.repositories.UserRepository;

import javax.persistence.EntityNotFoundException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@AllArgsConstructor
public class UserService implements UserDetailsService {

    private final UserRepository repository;
    private final BCryptPasswordEncoder passwordEncoder;

    /**
     * Получаем необходимые данные о пользователе для модуля аутентификации
     *
     * @param username никнейм пользователя
     *
     * @return интерфейс с данными пользователя
     *
     * @throws EntityNotFoundException пользователь не найден
     */
    @Override
    @Cacheable("userDetails")
    public UserDetails loadUserByUsername(String username) throws EntityNotFoundException {
        return findUserByEmail(username);
    }

    /**
     * Получение пользователя по айди
     *
     * @param id айди нужного нам пользователя
     *
     * @return возвращаем найденного пользователя
     */

    public UserDto getUserDtoById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Неверный идентификатор пользователя");
        }
        return repository.findById(id)
                .map(UserService::buildDto)
                .orElse(null);
    }

    /**
     * Редактирование пользователя
     *
     * @param newUserData новые данные пользователя
     *
     * @return возвращает ДТО пользователя
     */
    @CachePut(value = "findUserByUsername", key = "#user.username")
    public UserDto editUserDto(Long userId, UserDto newUserData) {
        User editedUser = this.findUserById(userId);

        editedUser.setFirstName(newUserData.getFirstName());
        editedUser.setLastName(newUserData.getLastName());
        editedUser.setMiddleName(newUserData.getMiddleName());
        editedUser.setEmail(newUserData.getEmail());
        editedUser.setPassport(newUserData.getPassport());
        editedUser.setPhoneNumber(newUserData.getPhoneNumber());
        editedUser.setPassword(newUserData.getPassport());

        return buildDto(repository.save(editedUser));
    }

    /**
     * Получение пользователя по его электронной почте
     *
     * @param email эл. почта пользователя
     *
     * @return данные о пользователе
     */
    @Cacheable(value = "findUserByEmail", key = "#email")
    public User findUserByEmail(String email) {
        return repository
                .findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Пользователь с email = %s не найден", email)));
    }

    /**
     * Получение пользователя по айди
     *
     * @param id айди пользователя
     *
     * @return возвращаем данные о пользователе
     */
    public User findUserById(Long id) {
        return repository
                .findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Пользователь с id = %s не найден", id)));
    }

    /**
     * Создание пользователя
     *
     * @param dto     данные пользователя для аутентификации
     * @param isAdmin признак администратора
     */
    @CachePut(value = "findUserByEmail", key = "#dto.email")
    public User createNonConfirmedUser(UserDto dto, boolean isAdmin) {
        if (repository.findByEmail(dto.getEmail()).isEmpty()) {
            Set<User.Role> roles = new HashSet<>();
            if (isAdmin) {
                roles.add(User.Role.ADMIN);
            } else {
                roles.add(User.Role.NON_CONFIRMED);
            }

            var user = new User();
            user.setFirstName(dto.getFirstName());
            user.setLastName(dto.getLastName());
            user.setMiddleName(dto.getMiddleName());
            user.setPassport(dto.getPassport());
            user.setPassword(passwordEncoder.encode(dto.getPassword()));
            user.setEmail(dto.getEmail());
            user.setPhoneNumber(dto.getPhoneNumber());
            user.setActive(true);
            user.setRoles(roles);

            return repository.save(user);
        } else {
            throw new UserExistsException(String.format("Пользователь с email %s уже существует!", dto.getEmail()));
        }
    }

    /**
     * Удаление пользователя по айди
     *
     * @param id айди пользователя
     */
    public void deleteUser(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Неверный идентификатор пользователя");
        }
        repository.deleteById(id);
    }

    @Transactional
    public void setStatus(Long id, boolean isActive) {
        repository.setActiveUser(id, isActive);
    }

    public void activateUser(Long id) {
        User userById = this.findUserById(id);

        if (userById.getRoles().contains(User.Role.NON_CONFIRMED)) {
            userById.setRoles(Set.of(User.Role.USER));
            repository.save(userById);
        } else {
            throw new IllegalArgumentException("Данного пользователя невозможно активировать");
        }
    }

    public boolean checkConfirmed(Long userId) {
        return repository.checkConfirmed(userId);
    }

    public List<UserDto> getAllUsersWithoutAdmins(String role) {
        return repository.findAllUsersWithoutAdmins(role).stream().map(UserService::buildDto).toList();
    }

    /**
     * Превратить сущность в ДТО
     *
     * @param user сущность Пользователь
     *
     * @return ДТО с данными
     */
    private static UserDto buildDto(User user) {
        return UserDto.builder()
                .id(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .middleName(user.getMiddleName())
                .email(user.getEmail())
                .phoneNumber(user.getPhoneNumber())
                .passport(user.getPassport())
                .password(user.getPassword())
                .active(user.isActive())
                .roles(user.getRoles())
                .build();
    }
}
