package ru.team418.financialapp.service.business;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.team418.financialapp.dto.BalanceOperationWithAccountDto;
import ru.team418.financialapp.entities.Account;
import ru.team418.financialapp.entities.Order;

import javax.transaction.Transactional;
import java.math.BigDecimal;

@Service
@AllArgsConstructor
@Slf4j
public class UnitOperations {

    private final AccountService accountService;

    @Transactional
    public void processBuyOperation(Order order) {
        Account senderAcc = accountService.getAccountById(order.getAccountSender().getId());
        Account receiverAcc = accountService.getAccountById(order.getAccountReceiver().getId());

        BigDecimal sum = order.getExchangeRate().multiply(order.getAmount());
        accountService.writeOffMoneyFromAccount(new BalanceOperationWithAccountDto(senderAcc.getId(), sum));
        accountService.depositAccount(new BalanceOperationWithAccountDto(receiverAcc.getId(), order.getAmount()));
    }

    @Transactional
    public void processSellOperation(Order order) {
        Account senderAcc = accountService.getAccountById(order.getAccountSender().getId());
        Account receiverAcc = accountService.getAccountById(order.getAccountReceiver().getId());

        BigDecimal sum = order.getExchangeRate().multiply(order.getAmount());
        accountService.writeOffMoneyFromAccount(new BalanceOperationWithAccountDto(senderAcc.getId(), order.getAmount()));
        accountService.depositAccount(new BalanceOperationWithAccountDto(receiverAcc.getId(), sum));
    }
}
