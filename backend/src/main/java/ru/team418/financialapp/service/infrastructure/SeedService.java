package ru.team418.financialapp.service.infrastructure;

import lombok.AllArgsConstructor;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import ru.team418.financialapp.dto.BalanceOperationWithAccountDto;
import ru.team418.financialapp.dto.UserDto;
import ru.team418.financialapp.dto.quotes.timeseries.TimeSeriesValueDto;
import ru.team418.financialapp.entities.Account.Currency;
import ru.team418.financialapp.entities.User;
import ru.team418.financialapp.entities.Order;
import ru.team418.financialapp.entities.Order.OrderStatus;
import ru.team418.financialapp.entities.Order.OrderType;
import ru.team418.financialapp.entities.Unit;
import ru.team418.financialapp.exceptions.UserExistsException;
import ru.team418.financialapp.repositories.OrderRepository;
import ru.team418.financialapp.repositories.UnitRepository;
import ru.team418.financialapp.service.business.AccountService;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;

/**
 * Сервис, предзаполняющий БД тестовыми данными при первом старте приложения
 */
@Service
@AllArgsConstructor
public class SeedService {

    private final UserService userService;
    private final UnitRepository unitRepository;

    private final AccountService accountService;
    private final OrderRepository orderRepository;

    @EventListener
    public void seed(ContextRefreshedEvent event) {
        seedNonConfirmedUser();
        seedActiveUser();
        seedBannedUser();
        seedAdmin();
        seedUnits();
        seedAccounts();
        seedOrders();
    }

    private void seedOrders() {
        if (!orderRepository.findAll().isEmpty()) {
            return;
        }

        Order order1 = new Order();
        order1.setType(OrderType.DEPOSIT);
        order1.setStatus(OrderStatus.PROCESSED);
        order1.setCurrency(Currency.RUB);
        order1.setAmount(new BigDecimal("6000"));
        order1.setAccountReceiver(accountService.getAccountById(1L));
        order1.setCreatedDate(LocalDateTime.now());
        order1.setUpdatedDate(LocalDateTime.now());

        Order order2 = new Order();
        order2.setType(OrderType.BUY);
        order2.setStatus(OrderStatus.PROCESSED);
        order2.setCurrency(Currency.USD);
        order2.setExchangeRate(new BigDecimal("70"));
        order2.setAmount(new BigDecimal("500"));
        order2.setAccountSender(accountService.getAccountById(1L));
        order2.setAccountReceiver(accountService.getAccountById(2L));
        order2.setCreatedDate(LocalDateTime.now());
        order2.setUpdatedDate(LocalDateTime.now());

        Order order3 = new Order();
        order3.setType(OrderType.BUY);
        order3.setStatus(OrderStatus.PROCESSED);
        order3.setCurrency(Currency.EUR);
        order3.setExchangeRate(new BigDecimal("75"));
        order3.setAmount(new BigDecimal("350"));
        order3.setAccountSender(accountService.getAccountById(1L));
        order3.setAccountReceiver(accountService.getAccountById(2L));
        order3.setCreatedDate(LocalDateTime.now());
        order3.setUpdatedDate(LocalDateTime.now());

        Order order4 = new Order();
        order4.setType(OrderType.BUY);
        order4.setStatus(OrderStatus.PROCESSED);
        order4.setCurrency(Currency.CAD);
        order4.setExchangeRate(new BigDecimal("46"));
        order4.setAmount(new BigDecimal("50"));
        order4.setAccountSender(accountService.getAccountById(1L));
        order4.setAccountReceiver(accountService.getAccountById(2L));
        order4.setCreatedDate(LocalDateTime.now());
        order4.setUpdatedDate(LocalDateTime.now());

        Order order5 = new Order();
        order5.setType(OrderType.BUY);
        order5.setStatus(OrderStatus.PROCESSED);
        order5.setCurrency(Currency.JPY);
        order5.setExchangeRate(new BigDecimal("0.45"));
        order5.setAmount(new BigDecimal("1200"));
        order5.setAccountSender(accountService.getAccountById(1L));
        order5.setAccountReceiver(accountService.getAccountById(2L));
        order5.setCreatedDate(LocalDateTime.now());
        order5.setUpdatedDate(LocalDateTime.now());

        orderRepository.saveAll(asList(order1, order2, order3, order4, order5));
    }


    private void seedAccounts() {
        if (!accountService.findAll().isEmpty()) {
            return;
        }
        accountService.createAccount(2L, Currency.RUB.toString());
        accountService.depositAccount(2L, new BalanceOperationWithAccountDto(1L, BigDecimal.valueOf(5000.00)));
        accountService.createAccount(2L, Currency.USD.toString());
        accountService.depositAccount(2L, new BalanceOperationWithAccountDto(2L, BigDecimal.valueOf(500.00)));
        accountService.createAccount(2L, Currency.EUR.toString());
        accountService.depositAccount(2L, new BalanceOperationWithAccountDto(3L, BigDecimal.valueOf(350.00)));
        accountService.createAccount(2L, Currency.CAD.toString());
        accountService.depositAccount(2L, new BalanceOperationWithAccountDto(4L, BigDecimal.valueOf(50.00)));
        accountService.createAccount(2L, Currency.JPY.toString());
        accountService.depositAccount(2L, new BalanceOperationWithAccountDto(5L, BigDecimal.valueOf(1200.00)));
        accountService.createAccount(3L, Currency.RUB.toString());
    }


    private void seedUnits() {
        if (!unitRepository.findAll().isEmpty()) {
            return;
        }
        List<Unit> units = Arrays
                .stream(Currency.values())
                .map(currency -> {
                    var unit = new Unit();
                    unit.setCurrency(currency);
                    unit.setExchangeRate(BigDecimal.ZERO);
                    unit.setCreatedDate(LocalDateTime.now());
                    unit.setMinutelyActualQuoteData(Collections.singletonList(new TimeSeriesValueDto()));
                    unit.setDailyActualQuoteData(Collections.singletonList(new TimeSeriesValueDto()));
                    return unit;
                }).toList();

        unitRepository.saveAll(units);
    }

    private void seedNonConfirmedUser() {
        String email = "nonConfirmed-user-1@yandex.ru";

        try {
            userService.findUserByEmail(email);
        } catch (EntityNotFoundException e) {
            UserDto user = UserDto.builder()
                    .firstName("Неподтвержденный")
                    .lastName("Василий")
                    .middleName("Петрович")
                    .email(email)
                    .phoneNumber("79191455698")
                    .passport("1234567811")
                    .password(email)
                    .build();
            userService.createNonConfirmedUser(user, false);
        } catch (UserExistsException ignored) {
        }
    }

    private void seedActiveUser() {
        String email = "activated-user-1@yandex.ru";

        try {
            userService.findUserByEmail(email);
        } catch (EntityNotFoundException e) {
            UserDto user = UserDto.builder()
                    .firstName("Активированный")
                    .lastName("Клиент")
                    .middleName("Портфельевич")
                    .email(email)
                    .phoneNumber("79786663399")
                    .passport("0914715236")
                    .password(email)
                    .build();
            User createdUser = userService.createNonConfirmedUser(user, false);
            userService.activateUser(createdUser.getId());
        } catch (UserExistsException ignored) {
        }
    }

    private void seedBannedUser() {
        String email = "banned-user-1@yandex.ru";

        try {
            userService.findUserByEmail(email);
        } catch (EntityNotFoundException e) {
            UserDto user = UserDto.builder()
                    .firstName("Забаненный")
                    .lastName("Клиент")
                    .middleName("Олегович")
                    .email(email)
                    .phoneNumber("79890002121")
                    .passport("1456214858")
                    .password(email)
                    .build();
            User createdUser = userService.createNonConfirmedUser(user, false);
            userService.activateUser(createdUser.getId());
            userService.setStatus(createdUser.getId(), false);
        } catch (UserExistsException ignored) {

        }
    }

    private void seedAdmin() {
        String email = "admin-1@yandex.ru";

        try {
            userService.findUserByEmail(email);
        } catch (EntityNotFoundException e) {
            UserDto user = UserDto.builder()
                    .firstName("Богоподобный")
                    .lastName("Админ")
                    .middleName("Админович")
                    .email(email)
                    .phoneNumber("79641234567")
                    .passport("3621852471")
                    .password(email)
                    .build();

            userService.createNonConfirmedUser(user, true);
        } catch (UserExistsException ignored) {

        }
    }
}
