package ru.team418.financialapp.service.business;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.team418.financialapp.dto.OrderDto;
import ru.team418.financialapp.dto.units.UnitDto;
import ru.team418.financialapp.entities.Account;
import ru.team418.financialapp.entities.Order;
import ru.team418.financialapp.entities.Order.OrderStatus;
import ru.team418.financialapp.entities.Order.OrderType;
import ru.team418.financialapp.exceptions.CreateOrderException;
import ru.team418.financialapp.repositories.OrderRepository;
import ru.team418.financialapp.utils.MessageService;

import javax.jms.JMSException;
import java.time.LocalDateTime;
import java.util.List;

import static java.util.Arrays.asList;

@Service
@AllArgsConstructor
@Slf4j
public class OrdersService {

    private final AccountService accountService;
    private final OrderRepository orderRepository;

    private final MessageService messageService;
    private final UnitsService unitsService;

    public void createOrder(Long userId, OrderType orderType, OrderDto dto) {
        if (asList(OrderType.DEPOSIT, OrderType.WITHDRAW).contains(orderType)
                && Account.Currency.RUB != dto.getCurrency()) {
            throw new CreateOrderException("Вносить/снимать деньги возможно только с рублёвого счета");
        }

        if (asList(OrderType.BUY, OrderType.SELL).contains(orderType)
                && Account.Currency.RUB == dto.getCurrency()) {
            throw new CreateOrderException("Невозможно купить/продать валюту Рубль, воспользуйтесь операцией Пополнение/Снятие");
        }
        Account senderAcc = null;
        Account receiverAcc = null;
        UnitDto accountUnit = null;
        if (asList(OrderType.BUY, OrderType.SELL).contains(orderType)) {
            if (dto.getAccountSenderId().equals(dto.getAccountReceiverId())) {
                throw new CreateOrderException("Невозможно купить валюту на счёт, с которого происходит списание");
            }
            senderAcc = accountService.getAccountById(userId, dto.getAccountSenderId());
            receiverAcc = accountService.getAccountById(userId, dto.getAccountReceiverId());
            if (OrderType.BUY == orderType) {
                accountUnit = unitsService.updateUnitExchangeRate(receiverAcc);
            } else {
                accountUnit = unitsService.updateUnitExchangeRate(senderAcc);
            }
        } else if (OrderType.DEPOSIT == orderType) {
            receiverAcc = accountService.getAccountById(userId, dto.getAccountReceiverId());
        } else if (OrderType.WITHDRAW == orderType) {
            senderAcc = accountService.getAccountById(userId, dto.getAccountSenderId());
        }

        Order order = new Order();
        order.setType(orderType);
        order.setStatus(OrderStatus.WAITING);
        order.setCurrency(dto.getCurrency());
        order.setExchangeRate(accountUnit != null ? accountUnit.getExchangeRate() : null);
        order.setAmount(dto.getAmount());
        order.setAccountSender(senderAcc);
        order.setAccountReceiver(receiverAcc);
        order.setCreatedDate(LocalDateTime.now());

        Order savedOrder = orderRepository.save(order);

        try {
            messageService.sendMessageToQueue(savedOrder.getId());
        } catch (JMSException e) {
            log.error(e.getMessage());
            throw new CreateOrderException(String.format("Ошибка при размещении ордера id %s", savedOrder.getId()));
        }

    }

    public List<OrderDto> getOrders(Long userId) {
        return orderRepository.findAllByUserId(userId)
                .stream()
                .map(OrdersService::toDto)
                .toList();
    }

    private static OrderDto toDto(Order entity) {
        return OrderDto.builder()
                .id(entity.getId())
                .type(entity.getType())
                .status(entity.getStatus())
                .currency(entity.getCurrency())
                .exchangeRate(entity.getExchangeRate()).amount(entity.getAmount())
                .accountSenderId(entity.getAccountSender() != null ? entity.getAccountSender().getId() : null)
                .accountReceiverId(entity.getAccountReceiver() != null ? entity.getAccountReceiver().getId() : null)
                .createdDate(entity.getCreatedDate())
                .updatedDate(entity.getUpdatedDate()).build();
    }
}
