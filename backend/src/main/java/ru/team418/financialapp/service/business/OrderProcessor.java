package ru.team418.financialapp.service.business;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.team418.financialapp.dto.BalanceOperationWithAccountDto;
import ru.team418.financialapp.entities.Order;
import ru.team418.financialapp.exceptions.OrderProcessingException;
import ru.team418.financialapp.repositories.OrderRepository;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.TextMessage;
import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderProcessor {
    private final MessageConsumer messageConsumer;
    private final AccountService accountService;
    private final UnitOperations unitOperations;
    private final OrderRepository orderRepository;

    @Scheduled(fixedDelay = 3000)
    @Transactional
    public void processOrderMessages() {
        try {
            Message receive = messageConsumer.receive(3000);
            if (receive != null) {
                TextMessage textMessage = (TextMessage) receive;
                Long orderId = Long.parseLong(textMessage.getText());

                Order order = orderRepository.findById(orderId)
                        .orElseThrow(() -> new EntityNotFoundException(String.format("Ордер с id %s не найден", orderId)));
                log.info(String.format("Получен ордер id = %s, операция = %s", order.getId(), order.getType().toString()));
                order.setStatus(Order.OrderStatus.PROCESSING);
                order.setUpdatedDate(LocalDateTime.now());
                Order savedOrder = orderRepository.save(order);

                this.processOrder(savedOrder);

                log.info(String.format("Ордер id = %s обработан", order.getId()));
                order.setStatus(Order.OrderStatus.PROCESSED);
                order.setUpdatedDate(LocalDateTime.now());
                orderRepository.save(order);
            }
        } catch (JMSException e) {
            log.error(e.getMessage());
            throw new OrderProcessingException("Ошибка обработки ордера");
        }
    }

    private void processOrder(Order order) {
        switch (order.getType()) {
            case BUY -> unitOperations.processBuyOperation(order);
            case SELL -> unitOperations.processSellOperation(order);
            case DEPOSIT ->
                    accountService.depositAccount(new BalanceOperationWithAccountDto(order.getAccountReceiver().getId(), order.getAmount()));
            case WITHDRAW ->
                    accountService.writeOffMoneyFromAccount(new BalanceOperationWithAccountDto(order.getAccountSender().getId(), order.getAmount()));
            default -> throw new OrderProcessingException("Ошибка обработки ордера: неподдерживаемый тип операции");
        }
    }
}
