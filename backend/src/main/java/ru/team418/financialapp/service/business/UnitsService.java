package ru.team418.financialapp.service.business;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.team418.financialapp.collectors.QuotesCollector;
import ru.team418.financialapp.dto.quotes.exchangerates.ExchangeRatesDto;
import ru.team418.financialapp.dto.quotes.timeseries.TimeSeriesDataDto;
import ru.team418.financialapp.dto.quotes.timeseries.TimeSeriesValueDto;
import ru.team418.financialapp.dto.units.UnitDto;
import ru.team418.financialapp.dto.units.UnitExtendedDto;
import ru.team418.financialapp.entities.Account;
import ru.team418.financialapp.entities.Unit;
import ru.team418.financialapp.exceptions.DataCollectorException;
import ru.team418.financialapp.exceptions.UnitUpdateException;
import ru.team418.financialapp.repositories.UnitRepository;
import ru.team418.financialapp.utils.UnitsUtils;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.util.*;

@Service
@AllArgsConstructor
@Slf4j
public class UnitsService {
    private final UnitRepository repository;
    private final QuotesCollector quotesCollector;

    public UnitExtendedDto getUnitByCode(String code) {
        if (Account.Currency.fromValue(code) == null) {
            throw new IllegalArgumentException("Неверный или неподдерживаемый код валюты");
        }

        Unit unit = repository.findUnitByCurrency(code)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Условная единица с кодом %s не найдена", code)));
        try {
            updateMinutelyQuotesUnits(Collections.singletonList(code + "/" + Account.Currency.RUB), Collections.singletonList(unit), true);
        } catch (DataCollectorException e) {
            log.error(e.getMessage());
            throw new UnitUpdateException(e.getMessage() + ". Попробуйте через минуту");
        }

        return UnitExtendedDto.builder()
                .id(unit.getId())
                .exchangeRate(unit.getExchangeRate())
                .fullName(unit.getCurrency().getFullName())
                .currency(unit.getCurrency())
                .updatedDate(unit.getUpdatedDate())
                .minutelyQuoteData(unit.getMinutelyActualQuoteData())
                .dailyQuoteData(unit.getDailyActualQuoteData())
                .build();
    }

    public UnitDto updateUnitExchangeRate(Account account) {
        if (account.getCurrency() == Account.Currency.RUB) {
            return new UnitDto();
        }
        List<String> currencyPairs = Collections.singletonList(account.getCurrency().toString() + "/" + Account.Currency.RUB);

        Unit entity = repository.findUnitByCurrency(account.getCurrency().toString())
                .orElseThrow(() -> new EntityNotFoundException("Данные по условной единице не найдены"));

        try {
            updateExchangeRate(currencyPairs, Collections.singletonList(entity), false);
        } catch (DataCollectorException e) {
            log.error(e.getMessage());
            throw new UnitUpdateException(e.getMessage() + ". Попробуйте через минуту");
        }

        Unit saved = repository.save(entity);
        return UnitDto.builder()
                .id(saved.getId())
                .exchangeRate(saved.getExchangeRate())
                .fullName(saved.getCurrency().getFullName())
                .currency(saved.getCurrency())
                .updatedDate(saved.getUpdatedDate())
                .build();
    }

    public List<UnitDto> getAllUnits_updateOnlyExchangeRate() {
        List<String> currencyString = Arrays.stream(Account.Currency.values())
                .filter(value -> Account.Currency.RUB != value)
                .map(Account.Currency::toString)
                .toList();

        List<String> currencyPairs = currencyString
                .stream()
                .map(currency -> currency + "/" + Account.Currency.RUB)
                .toList();

        List<Unit> allUnits = repository.findAll();

        try {
            updateExchangeRate(currencyPairs, allUnits, false);
        } catch (DataCollectorException e) {
            log.error(e.getMessage());
            throw new UnitUpdateException(e.getMessage() + ". Попробуйте через минуту");
        }

        return repository.saveAll(allUnits)
                .stream()
                .map(entity -> {
                    UnitDto dto = UnitDto.builder()
                            .id(entity.getId())
                            .exchangeRate(entity.getExchangeRate())
                            .fullName(entity.getCurrency().getFullName())
                            .currency(entity.getCurrency())
                            .updatedDate(entity.getUpdatedDate())
                            .build();
                    return dto;
                }).toList();
    }


    public List<UnitExtendedDto> getAllUnitsExtended() {
        List<String> currencyString = Arrays.stream(Account.Currency.values())
                .filter(value -> Account.Currency.RUB != value)
                .map(Account.Currency::toString)
                .toList();

        List<String> currencyPairs = currencyString
                .stream()
                .map(currency -> currency + "/" + Account.Currency.RUB)
                .toList();

        List<Unit> allUnits = repository.findAll();

        try {
            updateMinutelyQuotesUnits(currencyPairs, allUnits, false);
            updateDailyQuotesUnits(currencyPairs, allUnits, false);
            updateExchangeRate(currencyPairs, allUnits, false);
        } catch (DataCollectorException e) {
            log.error(e.getMessage());
            throw new UnitUpdateException(e.getMessage() + ". Попробуйте через минуту");
        }

        return repository.saveAll(allUnits)
                .stream()
                .map(entity -> {
                    UnitExtendedDto dto = UnitExtendedDto.builder()
                            .id(entity.getId())
                            .exchangeRate(entity.getExchangeRate())
                            .fullName(entity.getCurrency().getFullName())
                            .currency(entity.getCurrency())
                            .updatedDate(entity.getUpdatedDate())
                            .minutelyQuoteData(entity.getMinutelyActualQuoteData())
                            .dailyQuoteData(entity.getDailyActualQuoteData())
                            .build();
                    return dto;
                }).toList();
    }

    private void updateDailyQuotesUnits(List<String> currencyPairs, List<Unit> allUnits, boolean saveNow) throws DataCollectorException {
        Map<Unit, List<TimeSeriesValueDto>> dailyQuotesUnits = this.groupQuotes(currencyPairs, allUnits, "1month");

        if (!dailyQuotesUnits.isEmpty()) {
            dailyQuotesUnits.forEach((k, v) -> updateQuoteData(k, null, v, saveNow));
        } else {
            throw new UnitUpdateException("Не получены свежие данные об активе");
        }
    }

    private void updateMinutelyQuotesUnits(List<String> currencyPairs, List<Unit> allUnits, boolean saveNow) throws DataCollectorException {
        Map<Unit, List<TimeSeriesValueDto>> minutelyQuotesUnits = this.groupQuotes(currencyPairs, allUnits, "1min");

        if (!minutelyQuotesUnits.isEmpty()) {
            minutelyQuotesUnits.forEach((k, v) -> updateQuoteData(k, v, null, saveNow));
        } else {
            throw new UnitUpdateException("Не получены свежие данные об активе");
        }
    }

    private void updateExchangeRate(List<String> currencyPairs, List<Unit> allUnits, boolean saveNow) throws DataCollectorException {
        Map<Unit, ExchangeRatesDto> groupExchangeRates = this.groupExchangeRates(currencyPairs, allUnits);

        if (!groupExchangeRates.isEmpty()) {
            groupExchangeRates.forEach((k, v) -> updateExchangeRates(k, new BigDecimal(v.getRate()), Long.parseLong(v.getTimestamp()), saveNow));
        } else {
            throw new UnitUpdateException("Не получены свежие данные об активе");
        }
    }

    private Map<Unit, List<TimeSeriesValueDto>> groupQuotes(List<String> currencyPairs, List<Unit> allUnits, String interval) throws DataCollectorException {
        Map<String, TimeSeriesDataDto> items = quotesCollector.getTimeSeries(currencyPairs, interval);

        Map<Unit, List<TimeSeriesValueDto>> dailyItemsQuotes = new HashMap<>();
        items.forEach((k, v) -> {
            String currencyFromPair = k.substring(0, 3);
            Unit unitEntity = allUnits.stream()
                    .filter(unit -> unit.getCurrency().toString().startsWith(currencyFromPair))
                    .findFirst()
                    .orElseThrow(() -> new EntityNotFoundException(String.format("Условная единица с кодом %s не найдена", currencyFromPair)));
            dailyItemsQuotes.put(unitEntity, v.getValues());
        });

        return dailyItemsQuotes;
    }

    private Map<Unit, ExchangeRatesDto> groupExchangeRates(List<String> currencyPairs, List<Unit> allUnits) throws DataCollectorException {
        Map<String, ExchangeRatesDto> items = quotesCollector.getExchangeRates(currencyPairs);

        Map<Unit, ExchangeRatesDto> exchangeRates = new HashMap<>();
        items.forEach((k, v) -> {
            String currencyFromPair = k.substring(0, 3);
            Unit unitEntity = allUnits.stream()
                    .filter(unit -> unit.getCurrency().toString().startsWith(currencyFromPair))
                    .findFirst()
                    .orElseThrow(() -> new EntityNotFoundException(String.format("Условная единица с кодом %s не найдена", currencyFromPair)));
            exchangeRates.put(unitEntity, v);
        });

        return exchangeRates;
    }

    private Unit updateQuoteData(Unit unit, List<TimeSeriesValueDto> minuteQuoteInfo, List<TimeSeriesValueDto> dailyQuoteInfo, boolean saveNow) {
        UnitsUtils.updateQuoteData(unit, minuteQuoteInfo, dailyQuoteInfo);
        if (saveNow) {
            return repository.save(unit);
        } else {
            return unit;
        }
    }

    private Unit updateExchangeRates(Unit unit, BigDecimal rate, long timestamp, boolean saveNow) {
        UnitsUtils.updateExchangeRates(unit, rate, timestamp);
        if (saveNow) {
            return repository.save(unit);
        } else {
            return unit;
        }
    }
}
