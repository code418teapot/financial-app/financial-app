package ru.team418.financialapp.exceptions;

/**
 * Ошибка при выполнении операций со счетом
 */
public class BalanceOperationException extends RuntimeException {

    public BalanceOperationException() {
        super();
    }

    public BalanceOperationException(String message) {
        super(message);
    }
}
