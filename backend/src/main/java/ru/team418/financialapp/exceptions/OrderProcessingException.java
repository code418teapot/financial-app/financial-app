package ru.team418.financialapp.exceptions;

/**
 * Ошибка при обработке ордера
 */
public class OrderProcessingException extends RuntimeException {

    public OrderProcessingException() {
        super();
    }

    public OrderProcessingException(String message) {
        super(message);
    }
}
