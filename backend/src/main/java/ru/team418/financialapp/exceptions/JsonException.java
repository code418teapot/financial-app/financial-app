package ru.team418.financialapp.exceptions;

/**
 * Ошибка конвертации объекта в Json или Json в объект
 */
public class JsonException extends RuntimeException {

    /**
     * Конструктор
     *
     * @param e - исключение
     */
    public JsonException(Exception e) {
        super(e);
    }
}
