package ru.team418.financialapp.exceptions;

/**
 * Исключение "Срок жизни кэша закончился"
 */
public class OutdatedCacheException extends RuntimeException {

    public OutdatedCacheException() {
        super();
    }

    public OutdatedCacheException(String message) {
        super(message);
    }
}
