package ru.team418.financialapp.exceptions;

/**
 * Ошибка при обновлении данных валюты
 */
public class UnitUpdateException extends RuntimeException {

    public UnitUpdateException() {
        super();
    }

    public UnitUpdateException(String message) {
        super(message);
    }
}
