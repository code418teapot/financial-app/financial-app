package ru.team418.financialapp.exceptions;

/**
 * Ошибка при создании ордера
 */
public class CreateOrderException extends RuntimeException {

    public CreateOrderException() {
        super();
    }

    public CreateOrderException(String message) {
        super(message);
    }
}
