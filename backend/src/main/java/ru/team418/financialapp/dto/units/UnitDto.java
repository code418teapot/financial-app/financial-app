package ru.team418.financialapp.dto.units;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import ru.team418.financialapp.entities.Account;
import ru.team418.financialapp.entities.Unit;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * A DTO for the {@link Unit} entity
 */

@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Данные о валюте")
public class UnitDto {

    @Schema(description = "Идентификатор")
    private Long id;
    @Schema(description = "Дата обновления курса")
    private LocalDateTime updatedDate;
    @Schema(description = "Валюта")
    private Account.Currency currency;
    @Schema(description = "Название")
    private String fullName;
    @Schema(description = "Рыночный курс")
    private BigDecimal exchangeRate;

}