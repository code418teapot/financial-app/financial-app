package ru.team418.financialapp.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Данные для операции с балансом валютного счета")
@Validated
public class BalanceOperationWithAccountDto {

    @Schema(description = "Идентификатор валютного счета")
    @NotNull
    private Long accountId;

    @Schema(description = "Сумма")
    private BigDecimal amount;
}
