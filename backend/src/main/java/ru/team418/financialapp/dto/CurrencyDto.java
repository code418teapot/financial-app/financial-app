package ru.team418.financialapp.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serial;
import java.io.Serializable;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Данные о валюте")
public class CurrencyDto implements Serializable {

    @Serial
    private static final long serialVersionUID = -4353435345353L;

    @Schema(description = "Тип")
    private String type;

    @Schema(description = "Код")
    private String code;

    @Schema(description = "Полное название")
    private String fullName;

    @Schema(description = "Символ")
    private String symbol;
}
