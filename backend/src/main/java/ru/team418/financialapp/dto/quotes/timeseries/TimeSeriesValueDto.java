package ru.team418.financialapp.dto.quotes.timeseries;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Schema(description = "Запись истории котировки")
public class TimeSeriesValueDto implements Serializable {

    @JsonProperty("datetime")
    @Schema(description = "Время, на которую актуальна котировки")
    private String datetime;

    @JsonProperty("open")
    @Schema(description = "Цена открытия")
    private BigDecimal open;
    @JsonProperty("high")
    @Schema(description = "Высшая цена")
    private BigDecimal high;
    @JsonProperty("low")
    @Schema(description = "Низшая цена")
    private BigDecimal low;
    @JsonProperty("close")
    @Schema(description = "Цена закрытия")
    private BigDecimal close;


}
