package ru.team418.financialapp.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import ru.team418.financialapp.entities.Account;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * A DTO for the {@link Account} entity
 */
@Schema(description = "Данные о счете")
public record AccountDto(@Schema(description = "Идентификатор") Long id,
                         @Schema(description = "Дата создания") LocalDateTime createdDate,
                         @Schema(description = "Дата обновления") LocalDateTime updatedDate,
                         @Schema(description = "Валюта счета") Account.Currency currency,
                         @Schema(description = "Номер счета") String number,
                         @Schema(description = "Баланс в валюте") BigDecimal balance,
                         @Schema(description = "Баланс в рублях") BigDecimal rubBalance) implements Serializable {
}