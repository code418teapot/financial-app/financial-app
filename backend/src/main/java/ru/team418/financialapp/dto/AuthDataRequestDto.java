package ru.team418.financialapp.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.io.Serial;
import java.io.Serializable;

/**
 * Данные пользователя для аутентификации
 */
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Validated
@Schema(description = "Данные пользователя для аутентификации")
public class AuthDataRequestDto implements Serializable {

    @Serial
    private static final long serialVersionUID = 599053845034850L;

    @NotNull
    @Email
    @Schema(description = "Номер телефона")
    private String email;

    @NotNull
    @Schema(description = "Пароль")
    private String password;
}
