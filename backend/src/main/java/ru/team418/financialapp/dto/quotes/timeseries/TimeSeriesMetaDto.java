package ru.team418.financialapp.dto.quotes.timeseries;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class TimeSeriesMetaDto {

    @JsonProperty("symbol")
    private String symbol;

    @JsonProperty("interval")
    private String interval;

    @JsonProperty("currency_base")
    private String currencyBase;

    @JsonProperty("currency_quote")
    private String currencyQuote;

    @JsonProperty("type")
    private String type;

}
