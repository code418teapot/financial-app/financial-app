package ru.team418.financialapp.dto.quotes.timeseries;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class TimeSeriesDataDto {

    @JsonProperty("meta")
    private TimeSeriesMetaDto meta;

    @JsonProperty("values")
    private List<TimeSeriesValueDto> values;

    @JsonProperty("status")
    private String status;

}