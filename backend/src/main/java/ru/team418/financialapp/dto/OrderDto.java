package ru.team418.financialapp.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;
import ru.team418.financialapp.entities.Account;
import ru.team418.financialapp.entities.Order;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.io.Serial;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * A DTO for the {@link Order} entity
 */
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Validated
@Schema(description = "Данные ордера")
public class OrderDto implements Serializable {
    @Serial
    private static final long serialVersionUID = -4353435345353L;

    @Schema(description = "Идентификатор")
    private Long id;

    @Schema(description = "Дата создания")
    private LocalDateTime createdDate;

    @Schema(description = "Дата обновления")
    private LocalDateTime updatedDate;

    @Schema(description = "Тип")
    private Order.OrderType type;

    @Schema(description = "Статус")
    private Order.OrderStatus status;
    @NotNull
    @Schema(description = "Валюта")
    private Account.Currency currency;
    @Schema(description = "Рыночный курс")
    private BigDecimal exchangeRate;
    @Positive
    @Schema(description = "Количество")
    private BigDecimal amount;
    @Schema(description = "Идентификатор счета отправителя/списания")
    private Long accountSenderId;
    @Schema(description = "Идентификатор счета получателя/начисления")
    private Long accountReceiverId;
}