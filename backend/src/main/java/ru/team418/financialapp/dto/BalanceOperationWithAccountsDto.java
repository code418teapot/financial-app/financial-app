package ru.team418.financialapp.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Данные для операции с балансом валютных счетов")
@Validated
public class BalanceOperationWithAccountsDto {

    @Schema(description = "Данные счета отправителя")
    @NotNull
    private BalanceOperationWithAccountDto sender;

    @Schema(description = "Данные счета получателя")
    @NotNull
    private BalanceOperationWithAccountDto receiver;
}
