package ru.team418.financialapp.dto.units;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import ru.team418.financialapp.dto.quotes.timeseries.TimeSeriesValueDto;

import java.util.List;

@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Данные о валюте с котировками")
public class UnitExtendedDto extends UnitDto {

    @Schema(description = "Поминутные котировки")
    private List<TimeSeriesValueDto> minutelyQuoteData;

    @Schema(description = "Дневные котировки")
    private List<TimeSeriesValueDto> dailyQuoteData;

}
