package ru.team418.financialapp.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.io.Serial;
import java.io.Serializable;
import java.util.UUID;

/**
 * Ответ при аутентификации пользователя в системе
 */
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Токены аутентификации пользователя в системе")
public class AuthTokensDto implements Serializable {

    @Serial
    private static final long serialVersionUID = -4353435345353L;

    @Schema(description = "Токен аутентификации")
    private String accessToken;

    @Schema(description = "Refresh-токен")
    @NotBlank
    private UUID refreshToken;

    @Schema(description = "Время деактивации токена аутентификации")
    private long expiresIn;
}
