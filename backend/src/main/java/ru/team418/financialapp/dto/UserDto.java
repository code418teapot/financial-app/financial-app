package ru.team418.financialapp.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import org.springframework.validation.annotation.Validated;
import ru.team418.financialapp.entities.User;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serial;
import java.io.Serializable;
import java.util.Set;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Validated
@Schema(description = "Данные пользователя")
public class UserDto implements Serializable {

    @Serial
    private static final long serialVersionUID = 599053845034850L;

    @Schema(description = "Идентификатор")
    private Long id;

    @Schema(description = "Имя")
    @NotBlank
    private String firstName;

    @Schema(description = "Фамилия")
    @NotBlank
    private String lastName;

    @Schema(description = "Отчество")
    private String middleName;

    @Email
    @NotBlank
    @Schema(description = "Электронная почта")
    private String email;

    @Schema(description = "Номер телефона")
    @NotBlank
    private String phoneNumber;

    @Schema(description = "Серия и номер паспорта")
    @NotBlank
    private String passport;

    @NotNull
    @Schema(description = "Пароль")
    private String password;

    @Schema(description = "Признак активности/бана пользователя")
    private boolean active;

    @Schema(description = "Список ролей")
    private Set<User.Role> roles;
}
