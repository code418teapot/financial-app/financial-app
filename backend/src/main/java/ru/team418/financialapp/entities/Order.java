package ru.team418.financialapp.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Arrays;

@Entity
@Table(name = "orders")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Order extends BaseEntity {

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private OrderType type;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private OrderStatus status;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Account.Currency currency;

    @Column(name = "exchange_rate")
    private BigDecimal exchangeRate;

    @Column(name = "amount")
    private BigDecimal amount;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(columnDefinition = "integer", name = "account_sender_id")
    private Account accountSender;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(columnDefinition = "integer", name = "account_receiver_id")
    private Account accountReceiver;

    public enum OrderType {
        BUY("BUY"),
        SELL("SELL"),
        DEPOSIT("DEPOSIT"),
        WITHDRAW("WITHDRAW");

        private final String type;

        OrderType(String type) {
            this.type = type;
        }


        @Override
        @JsonValue
        public String toString() {
            return type;
        }

        /**
         * Получить значение енума из строки
         *
         * @param text строковое представление значения енума (напр. "BUY")
         *
         * @return значение енума (OrderType.BUY)
         */
        @JsonCreator
        public static Order.OrderType fromValue(String text) {
            return Arrays.stream(Order.OrderType.values())
                    .filter(candidate -> candidate.type.equals(text))
                    .findFirst()
                    .orElse(null);
        }
    }

    public enum OrderStatus {
        WAITING("WAITING"),
        PROCESSING("PROCESSING"),
        PROCESSED("PROCESSED");

        private final String type;

        OrderStatus(String type) {
            this.type = type;
        }


        @Override
        @JsonValue
        public String toString() {
            return type;
        }

        /**
         * Получить значение енума из строки
         *
         * @param text строковое представление значения енума (напр. "WAITING")
         *
         * @return значение енума (OrderStatus.WAITING)
         */
        @JsonCreator
        public static Order.OrderStatus fromValue(String text) {
            return Arrays.stream(Order.OrderStatus.values())
                    .filter(candidate -> candidate.type.equals(text))
                    .findFirst()
                    .orElse(null);
        }
    }

}
