package ru.team418.financialapp.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;
import java.util.Arrays;

@Entity
@Table(name = "accounts")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Account extends BaseEntity {

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(columnDefinition = "integer", name = "user_id")
    private User user;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "currency")
    private Currency currency;

    @NotBlank
    @Column(name = "number")
    private String number;

    @NotNull
    @PositiveOrZero
    @Column(name = "balance")
    private BigDecimal balance;

    public void addBalance(BigDecimal amount) {
        this.balance = this.balance.add(amount);
    }

    public void writeOffBalance(BigDecimal amount) {
        this.balance = balance.subtract(amount);
    }

    public enum Currency {
        RUB("RUB", "643", "Российский рубль", "₽"),
        USD("USD", "840", "Доллар США", "$"),
        EUR("EUR", "978", "Евро", "€"),
        CNY("CNY", "156", "Китайский юань", "¥"),
        JPY("JPY", "392", "Японская йена", "¥"),
        CAD("CAD", "124", "Канадский доллар", "С$");

        private final String type;
        private final String accountCode;
        private final String fullName;
        private final String symbol;

        Currency(String type, String accountCode, String fullName, String symbol) {
            this.type = type;
            this.accountCode = accountCode;
            this.fullName = fullName;
            this.symbol = symbol;
        }

        public String getAccountCode() {
            return accountCode;
        }

        public String getFullName() {
            return fullName;
        }

        public String getSymbol() {
            return symbol;
        }

        @Override
        @JsonValue
        public String toString() {
            return type;
        }

        /**
         * Получить значение енума из строки
         *
         * @param text строковое представление значения енума (напр. "RUB")
         *
         * @return значение енума (Currency.RUB)
         */
        @JsonCreator
        public static Currency fromValue(String text) {
            return Arrays.stream(Currency.values())
                    .filter(candidate -> candidate.type.equals(text))
                    .findFirst()
                    .orElse(null);
        }
    }

}
