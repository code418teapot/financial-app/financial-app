package ru.team418.financialapp.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.team418.financialapp.converters.QuotesJsonConverter;
import ru.team418.financialapp.dto.quotes.timeseries.TimeSeriesValueDto;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "units")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Unit extends BaseEntity {

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "currency")
    private Account.Currency currency;

    @PositiveOrZero
    @Column(name = "exchange_rate")
    private BigDecimal exchangeRate;

    @Convert(converter = QuotesJsonConverter.class)
    @Column(columnDefinition = "jsonb", name = "minutely_quote_data")
    private List<TimeSeriesValueDto> minutelyActualQuoteData;

    @Convert(converter = QuotesJsonConverter.class)
    @Column(columnDefinition = "jsonb", name = "daily_quote_data")
    private List<TimeSeriesValueDto> dailyActualQuoteData;

}
