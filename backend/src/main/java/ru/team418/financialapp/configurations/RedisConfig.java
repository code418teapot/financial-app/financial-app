package ru.team418.financialapp.configurations;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisClientConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import redis.clients.jedis.Jedis;

@Configuration
public class RedisConfig {

    public static final String EXCHANGE_RATES_REDIS_CACHE = "exchangeRates";
    public static final String EXCHANGE_RATES_REDIS_CACHE_KEY = EXCHANGE_RATES_REDIS_CACHE.concat(":%s");
    public static final String QUOTES_REDIS_CACHE = "quotes";
    public static final String QUOTES_REDIS_CACHE_KEY = QUOTES_REDIS_CACHE.concat(":%s:%s");
    @Value("${redis.host}")
    private String host;

    @Value("${redis.port}")
    private int port;

    @Bean
    public JedisConnectionFactory jedisConnectionFactory() {
        RedisStandaloneConfiguration redisConfiguration = new RedisStandaloneConfiguration();
        redisConfiguration.setHostName(host);
        redisConfiguration.setPort(port);

        JedisClientConfiguration.JedisClientConfigurationBuilder jedisClientConfiguration = JedisClientConfiguration.builder();

        return new JedisConnectionFactory(redisConfiguration, jedisClientConfiguration.build());
    }

    @Bean
    public RedisTemplate<String, Object> redisTemplate() {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(jedisConnectionFactory());
        return template;
    }

    @Bean
    public Jedis jedis(@Value("${redis.host}") final String host,
                       @Value("${redis.port}") final int port) {
        return new Jedis(host, port);
    }

}
