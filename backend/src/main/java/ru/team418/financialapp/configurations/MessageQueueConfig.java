package ru.team418.financialapp.configurations;

import com.amazon.sqs.javamessaging.AmazonSQSMessagingClientWrapper;
import com.amazon.sqs.javamessaging.ProviderConfiguration;
import com.amazon.sqs.javamessaging.SQSConnection;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.jms.*;

@Configuration
public class MessageQueueConfig {

    private static final String QUEUE_NAME = "finapp-queue-order-dev";

    @Value("${yandex.cloud.access-key}")
    private String accessKey;
    @Value("${yandex.cloud.secret-key}")
    private String secretKey;

    @Bean
    public Session messageQueueSessionBean() throws Exception {
        SQSConnectionFactory connectionFactory = new SQSConnectionFactory(
                new ProviderConfiguration(),
                AmazonSQSClientBuilder.standard()
                        .withCredentials(new AWSStaticCredentialsProvider(getAWSCredentials()))
                        .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(
                                "https://message-queue.api.cloud.yandex.net",
                                "ru-central1"
                        ))
        );

        SQSConnection connection = connectionFactory.createConnection();

        AmazonSQSMessagingClientWrapper client = connection.getWrappedAmazonSQSClient();

        if (!client.queueExists(QUEUE_NAME)) {
            client.createQueue(QUEUE_NAME);
        }
        connection.start();
        return connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
    }

    @Bean
    public Queue queueBean(Session session) throws JMSException {
        return session.createQueue(QUEUE_NAME);
    }

    @Bean
    public MessageProducer producerBean(Session session, Queue queue) throws JMSException {
        return session.createProducer(queue);
    }

    @Bean
    public MessageConsumer consumerBean(Session session, Queue queue) throws JMSException {
        return session.createConsumer(queue);
    }

    private AWSCredentials getAWSCredentials() {
        return new BasicAWSCredentials(
                accessKey,
                secretKey
        );
    }
}
