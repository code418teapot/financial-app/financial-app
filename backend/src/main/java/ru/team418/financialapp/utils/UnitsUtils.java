package ru.team418.financialapp.utils;

import ru.team418.financialapp.dto.quotes.timeseries.TimeSeriesValueDto;
import ru.team418.financialapp.entities.Unit;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;
import java.util.TimeZone;

public final class UnitsUtils {

    private UnitsUtils() {
    }

    public static void updateExchangeRates(Unit unit, BigDecimal rate, long timestamp) {
        unit.setExchangeRate(rate);
        unit.setUpdatedDate(LocalDateTime.ofInstant(Instant.ofEpochMilli(timestamp),
                TimeZone.getDefault().toZoneId()));
    }

    public static void updateQuoteData(Unit unit, List<TimeSeriesValueDto> minuteQuoteInfo, List<TimeSeriesValueDto> dailyQuoteInfo) {
        if (minuteQuoteInfo != null && !minuteQuoteInfo.isEmpty()) {
            unit.setMinutelyActualQuoteData(minuteQuoteInfo);
        }
        if (dailyQuoteInfo != null && !dailyQuoteInfo.isEmpty()) {
            unit.setDailyActualQuoteData(dailyQuoteInfo);
        }
    }

}
