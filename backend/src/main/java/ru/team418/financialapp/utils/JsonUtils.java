package ru.team418.financialapp.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import ru.team418.financialapp.exceptions.JsonException;

import java.io.IOException;

/**
 * Вспомогательный класс для работы с сериализацией/десериализацией java-объектов в JSON
 */
public final class JsonUtils {

    private JsonUtils() {
    }

    /**
     * Сериализация объекта
     *
     * @param obj - объект
     *
     * @return объект в JSON-формате
     */
    public static String asJsonString(final Object obj) {
        try {
            return getMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new JsonException(e);
        }
    }

    /**
     * Преобразует строку в объект
     *
     * @param json строка в формате JSON
     * @param type класс, используется для получения полной информации типа генериков
     * @param <T>  тип генериков
     *
     * @return преобразованный объект
     */
    public static <T> T fromJsonString(final String json, TypeReference<T> type) {
        try {
            return getMapper().readValue(json, type);
        } catch (Exception e) {
            throw new JsonException(e);
        }
    }

    /**
     * Преобразует объект в экземпляр класса
     *
     * @param obj   - объект
     * @param clazz - класс, экземпляр которого нужно получить
     * @param <T>   - тип генериков
     *
     * @return экземпляр класса
     */
    public static <T> T objectToClass(final Object obj, Class<T> clazz) {
        try {
            String json = getMapper().writeValueAsString(obj);
            return getMapper().readValue(json, clazz);
        } catch (IOException e) {
            throw new JsonException(e);
        }
    }

    /**
     * Преобразует объект в экземпляр класса
     *
     * @param json - строка
     * @param type - класс, экземпляр которого нужно получить
     * @param <T>  - тип генериков
     *
     * @return экземпляр класса
     */
    public static <T> T fromJsonString(final String json, Class<T> type) {
        try {
            return getMapper().readValue(json, type);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static ObjectMapper getMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        return mapper;
    }
}
