package ru.team418.financialapp.utils;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;

@Service
@RequiredArgsConstructor
public class MessageService {

    private final Session session;

    private final MessageProducer messageProducer;

    public void sendMessageToQueue(Long orderId) throws JMSException {
        Message message = session.createTextMessage(String.valueOf(orderId));

        messageProducer.send(message);
    }


}
