package ru.team418.financialapp.controllers;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.team418.financialapp.dto.UserDto;
import ru.team418.financialapp.service.infrastructure.UserService;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
@RequestMapping("/public/users")
@Tag(name = "Пользователи", description = "API управления пользователями системы")
public class PublicUserController {

    private final UserService userService;

    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(description = "Регистрация пользователя")
    public void register(@Parameter(description = "Данные пользователя") @Valid @RequestBody UserDto registerData) {
        userService.createNonConfirmedUser(registerData, false);
    }

    @PostMapping("/register/admin")
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(description = "Регистрация администратора")
    public void registerAdmin(@Parameter(description = "Данные администратора") @Valid @RequestBody UserDto registerData) {
        userService.createNonConfirmedUser(registerData, true);
    }

}
