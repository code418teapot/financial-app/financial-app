package ru.team418.financialapp.controllers;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.team418.financialapp.dto.OrderDto;
import ru.team418.financialapp.entities.Order;
import ru.team418.financialapp.service.business.OrdersService;

import static ru.team418.financialapp.advices.RequestFilter.USER_ID;

@RestController
@AllArgsConstructor
@RequestMapping("/protected/units/operations")
@Tag(name = "Операции с валютой (protected)", description = "API для операции с валютами от имени аутентифицированных пользователей")
public class ProtectedUnitOperationsController {

    private final OrdersService ordersService;

    @PostMapping("/buy")
    @Operation(description = "Выставить ордер на покупку валюты")
    public void buy(@RequestAttribute(USER_ID) Long userId, @RequestBody OrderDto dto) {
        ordersService.createOrder(userId, Order.OrderType.BUY, dto);
    }

    @PostMapping("/sell")
    @Operation(description = "Выставить ордер на продажу валюты")
    public void sell(@RequestAttribute(USER_ID) Long userId, @RequestBody OrderDto dto) {
        ordersService.createOrder(userId, Order.OrderType.SELL, dto);
    }


}
