package ru.team418.financialapp.controllers;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;
import ru.team418.financialapp.dto.AuthDataRequestDto;
import ru.team418.financialapp.dto.AuthTokensDto;
import ru.team418.financialapp.service.infrastructure.AccessTokenProvider;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@AllArgsConstructor
@RequestMapping("/public")
@Tag(name = "Аутентификация", description = "API модуля аутентификации в системе")
public class PublicAuthenticationController {

    private final AuthenticationManager authenticationManager;
    private final AccessTokenProvider accessTokenProvider;

    @PostMapping("/auth/login")
    @Operation(description = "Аутентификация пользователя")
    public AuthTokensDto login(@Parameter(description = "Данные пользователя для аутентификации") @Valid @RequestBody AuthDataRequestDto loginData) {
        String email = loginData.getEmail();
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, loginData.getPassword()));

        return accessTokenProvider.generateAllTokens(email);
    }

    @PostMapping("/auth/refresh")
    @Operation(description = "Получение новых токенов")
    public AuthTokensDto refreshToken(@Parameter(description = "Refresh-токен пользователя") @RequestBody AuthTokensDto refreshTokenData) {
        UUID refreshToken = refreshTokenData.getRefreshToken();

        return accessTokenProvider.regenerateAllTokens(refreshToken);
    }

    @PostMapping("/auth/logout")
    @Operation(description = "Выход из системы")
    public void logout(@Parameter(description = "Refresh-токен пользователя") @RequestBody AuthTokensDto refreshTokenData) {
        UUID refreshToken = refreshTokenData.getRefreshToken();
        accessTokenProvider.logoutUser(refreshToken);
    }
}
