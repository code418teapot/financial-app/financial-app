package ru.team418.financialapp.controllers;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.team418.financialapp.dto.OrderDto;
import ru.team418.financialapp.service.business.OrdersService;

import java.util.List;

import static ru.team418.financialapp.advices.RequestFilter.USER_ID;

@RestController
@AllArgsConstructor
@RequestMapping("/protected/orders")
@Tag(name = "История операций (protected)", description = "API получения истории операций от имени аутентифицированных пользователей")
public class ProtectedOrdersController {

    private final OrdersService ordersService;

    @GetMapping
    @Operation(description = "Получить историю операции клиента")
    public List<OrderDto> getOrdersHistory(@RequestAttribute(USER_ID) Long userId) {
        return ordersService.getOrders(userId);
    }

}
