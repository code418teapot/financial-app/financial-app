package ru.team418.financialapp.controllers;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.team418.financialapp.dto.CurrencyDto;
import ru.team418.financialapp.dto.units.UnitExtendedDto;
import ru.team418.financialapp.entities.Account;
import ru.team418.financialapp.service.business.UnitsService;

import java.util.Arrays;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/protected/units")
@Tag(name = "Валюты (protected)", description = "API управления валютами от имени аутентифицированных пользователей")
public class ProtectedUnitController {

    private final UnitsService unitsService;

    @GetMapping
    @Operation(description = "Получить актуальные данные о валютах")
    public List<UnitExtendedDto> getAllUnits() {
        return unitsService.getAllUnitsExtended();
    }

    @GetMapping("/list")
    @Operation(description = "Получить список валют")
    public List<CurrencyDto> getCurrenciesList() {
        return Arrays
                .stream(Account.Currency.values())
                .map(currency -> new CurrencyDto(currency.toString(), currency.getAccountCode(), currency.getFullName(), currency.getSymbol()))
                .toList();
    }


}
