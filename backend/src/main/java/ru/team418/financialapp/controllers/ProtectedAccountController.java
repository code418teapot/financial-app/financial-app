package ru.team418.financialapp.controllers;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.team418.financialapp.dto.AccountDto;
import ru.team418.financialapp.dto.BalanceOperationWithAccountsDto;
import ru.team418.financialapp.dto.OrderDto;
import ru.team418.financialapp.entities.Order;
import ru.team418.financialapp.service.business.AccountService;
import ru.team418.financialapp.service.business.OrdersService;

import javax.validation.Valid;
import java.util.List;

import static ru.team418.financialapp.advices.RequestFilter.USER_ID;

@RestController
@AllArgsConstructor
@RequestMapping("/protected/accounts")
@Tag(name = "Брокерские счета (protected)", description = "API управления брокерскими счетами от имени аутентифицированных пользователей")
public class ProtectedAccountController {

    private final AccountService accountService;
    private final OrdersService ordersService;

    @GetMapping(path = "/about")
    @Operation(description = "Получить данные о своих счетах")
    public List<AccountDto> getAccountsByClientId(@RequestAttribute(USER_ID) Long userId) {
        return accountService.getAccountsByClientId(userId);
    }

    @GetMapping(path = "/{id}")
    @Operation(description = "Получить данные о своем счете по id")
    public AccountDto getAccountById(@RequestAttribute(USER_ID) Long userId, @PathVariable("id") Long accountId) {
        return accountService.getAccountByIdDto(userId, accountId);
    }

    @PostMapping
    @Operation(description = "Открыть валютный счет")
    public AccountDto openAccount(@RequestAttribute(USER_ID) Long userId, @Parameter(description = "Валюта счета") @RequestParam String currency) {
        return accountService.createAccount(userId, currency);
    }

    @PostMapping("/deposit")
    @Operation(description = "Создать ордер на пополнение рублёвого счета")
    public void depositAccount(@RequestAttribute(USER_ID) Long userId, @Parameter(description = "Данные для операции с балансом рублёвого счета") @Valid @RequestBody OrderDto dto) {
        ordersService.createOrder(userId, Order.OrderType.DEPOSIT, dto);
    }

    @PostMapping("/writeOff")
    @Operation(description = "Списать с валютного счета")
    public void writeOffFromAccount(@RequestAttribute(USER_ID) Long userId, @Parameter(description = "Данные для операции с балансом рублёвого счета") @Valid @RequestBody OrderDto dto) {
        ordersService.createOrder(userId, Order.OrderType.WITHDRAW, dto);
    }

    @PostMapping("/transfer")
    @Operation(description = "Перевести на другой счет")
    public void transferMoney(@RequestAttribute(USER_ID) Long userId, @Parameter(description = "Данные для операции с балансом валютных счетов") @Valid @RequestBody BalanceOperationWithAccountsDto dto) {
        accountService.transferMoney(userId, dto);
    }

    @PostMapping("/close")
    @Operation(description = "Закрыть валютный счет")
    public void closeAccount(@RequestAttribute(USER_ID) Long userId, @Parameter(description = "Данные для операции с балансом валютных счетов") @Valid @RequestBody BalanceOperationWithAccountsDto dto) {
        accountService.closeAccount(userId, dto);
    }

}
