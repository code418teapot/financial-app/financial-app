package ru.team418.financialapp.controllers;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.team418.financialapp.dto.CurrencyDto;
import ru.team418.financialapp.dto.units.UnitExtendedDto;
import ru.team418.financialapp.entities.Account;
import ru.team418.financialapp.service.business.UnitsService;

import java.util.Arrays;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/public/units")
@Tag(name = "Валюты", description = "Публичное API управления валютами")
public class PublicUnitController {

    private final UnitsService unitsService;

    @GetMapping("/{textCode}")
    @Operation(description = "Получить актуальные данные о валюте")
    public UnitExtendedDto getUnit(@PathVariable("textCode") String textCode) {
        return unitsService.getUnitByCode(textCode);
    }


}
