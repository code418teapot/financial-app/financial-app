package ru.team418.financialapp.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/public")
@Tag(name = "Тестовый контроллер", description = "API для проверки работоспособности приложения")
public class PublicHelloWorldController {

    @GetMapping(path = "/hello")
    @Operation(description = "Тестовая страница")
    public String getPage() {
        return "Hello World!";
    }


}
