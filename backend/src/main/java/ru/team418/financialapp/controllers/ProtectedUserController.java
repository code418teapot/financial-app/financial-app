package ru.team418.financialapp.controllers;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.team418.financialapp.dto.UserDto;
import ru.team418.financialapp.service.infrastructure.UserService;

import javax.validation.Valid;

import static ru.team418.financialapp.advices.RequestFilter.USER_ID;

@RestController
@AllArgsConstructor
@RequestMapping("/protected/users")
@Tag(name = "Пользователи (protected)", description = "API управления пользователями системы от имени аутентифицированных пользователей")
public class ProtectedUserController {

    private final UserService userService;

    @GetMapping(path = "/about")
    @Operation(description = "Получить данные о себе")
    public UserDto getUserById(@RequestAttribute(USER_ID) Long userId) {
        return userService.getUserDtoById(userId);
    }

    @PatchMapping
    @Operation(description = "Редактирование пользователя")
    public UserDto editUserDto(@RequestAttribute(USER_ID) Long userId, @Parameter(description = "Данные пользователя") @Valid @RequestBody UserDto newUserData) {
        return userService.editUserDto(userId, newUserData);
    }

    @GetMapping("/isActivated")
    @Operation(description = "Проверить подтвержден ли пользователь")
    public boolean checkConfirmed(@RequestAttribute(USER_ID) Long userId) {
        return userService.checkConfirmed(userId);
    }

}
