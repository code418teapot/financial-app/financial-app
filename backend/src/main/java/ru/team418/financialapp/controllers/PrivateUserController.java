package ru.team418.financialapp.controllers;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.team418.financialapp.dto.UserDto;
import ru.team418.financialapp.entities.Account;
import ru.team418.financialapp.entities.User;
import ru.team418.financialapp.service.business.AccountService;
import ru.team418.financialapp.service.infrastructure.UserService;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/private/users")
@Tag(name = "Пользователи (private)", description = "API управления пользователями системы от имени администраторов")
public class PrivateUserController {

    private final UserService userService;
    private final AccountService accountService;

    @GetMapping
    @Operation(description = "Получить всех пользователей неадминов в системе")
    public List<UserDto> getUsersList() {
        return userService.getAllUsersWithoutAdmins(User.Role.ADMIN.toString());
    }

    @GetMapping(path = "/{id}")
    @Operation(description = "Получить пользователя по айди")
    public UserDto getUserById(@Parameter(description = "Идентификатор пользователя") @PathVariable("id") Long id) {
        return userService.getUserDtoById(id);
    }

    @PostMapping("/activate/{id}")
    @Operation(description = "Активировать пользователя")
    public void activateUser(@Parameter(description = "Идентификатор пользователя") @PathVariable("id") Long id) {
        userService.activateUser(id);
        accountService.createAccount(id, Account.Currency.RUB.toString());
    }


    @PutMapping("/status/{id}")
    @Operation(description = "Сменить статус пользователя")
    public void setStatus(@Parameter(description = "Идентификатор пользователя") @PathVariable("id") Long id, @Parameter(description = "Пользователь активен/заблокирован") @RequestParam("isActive") boolean isActive) {
        userService.setStatus(id, isActive);
    }

    @PatchMapping
    @Operation(description = "Редактирование пользователя")
    public UserDto editUserDto(@Parameter(description = "Данные пользователя") @Valid @RequestBody UserDto newUserData) {
        return userService.editUserDto(newUserData.getId(), newUserData);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(description = "Удаление пользователя")
    public void deleteUser(@Parameter(description = "Идентификатор пользователя") @RequestBody Long id) {
        userService.deleteUser(id);
    }

}
