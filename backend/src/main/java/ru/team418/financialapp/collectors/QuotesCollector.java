package ru.team418.financialapp.collectors;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import redis.clients.jedis.Jedis;
import ru.team418.financialapp.dto.quotes.exchangerates.ExchangeRatesDto;
import ru.team418.financialapp.dto.quotes.timeseries.TimeSeriesDataDto;
import ru.team418.financialapp.dto.quotes.timeseries.TimeSeriesValueDto;
import ru.team418.financialapp.exceptions.DataCollectorException;
import ru.team418.financialapp.exceptions.OutdatedCacheException;
import ru.team418.financialapp.utils.JsonUtils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static org.springframework.http.HttpStatus.OK;
import static ru.team418.financialapp.configurations.RedisConfig.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class QuotesCollector {
    private static final String SITE = "https://api.twelvedata.com";

    private final Jedis jedis;

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()
            .registerModule(new JavaTimeModule())
            .setDateFormat(new SimpleDateFormat("yyyy-MM-dd"))
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            .configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);

    @Value("${collectors.twelvedata.apiKey-quotes}")
    private String API_KEY_QUOTES;

    @Value("${collectors.twelvedata.apiKey-exchangeRates}")
    private String API_KEY_EXCHANGE_RATES;

    @SneakyThrows
    public Map<String, TimeSeriesDataDto> getTimeSeries(Collection<String> currencyPairs, String interval) throws DataCollectorException {
        Map<String, TimeSeriesDataDto> cache = getTimeSeriesCache(currencyPairs, interval);
        if (cache != null) {
            return cache;
        }

        Map<String, TimeSeriesDataDto> result = new HashMap<>();
        String symbols = String.join(",", currencyPairs);
        RestTemplate restTemplate = new RestTemplate();
        String url = SITE + "/time_series?symbol=" + symbols + ",&interval=" + interval + "&apikey=" + API_KEY_QUOTES + "&source=docs";
        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
        if (OK.equals(response.getStatusCode())) {
            currencyPairs.forEach(item -> {
                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(response.getBody());
                    JSONObject data = jsonObject.getJSONObject(item);
                    result.put(item, OBJECT_MAPPER.readValue(data.toString(), TimeSeriesDataDto.class));
                } catch (Exception e) {
                    try {
                        jsonObject = new JSONObject(response.getBody());
                        int status = jsonObject.getInt("code");
                        if (HttpStatus.TOO_MANY_REQUESTS.value() == status) {
                            throw new DataCollectorException("Достигнут лимит обращения к API TwelveData");
                        } else {
                            throw new DataCollectorException("Неизвестная ошибка при получении данных с TwelveData");
                        }
                    } catch (JSONException ex) {
                        log.error(ex.getMessage());
                        throw new DataCollectorException("Неизвестная ошибка при получении данных с TwelveData");
                    }
                }
            });
        } else {
            throw new DataCollectorException("Ошибка при сборе данных об котировках валют коллектором TwelveData");
        }

        result.forEach((k, v) -> {
            Map<String, String> forCache = new HashMap<>();
            forCache.put(k, JsonUtils.asJsonString(v.getValues()));
            this.jedis.hmset(String.format(QUOTES_REDIS_CACHE_KEY, k, interval), forCache);
            this.jedis.sadd(QUOTES_REDIS_CACHE, v.getMeta().getSymbol());
        });

        return result;
    }

    @SneakyThrows
    public Map<String, ExchangeRatesDto> getExchangeRates(Collection<String> currencyPairs) throws DataCollectorException {
        Map<String, ExchangeRatesDto> cache = getExchangeRatesCache(currencyPairs);
        if (cache != null) {
            return cache;
        }

        Map<String, ExchangeRatesDto> result = new HashMap<>();
        String symbols = String.join(",", currencyPairs);
        RestTemplate restTemplate = new RestTemplate();
        String url = SITE + "/exchange_rate?symbol=" + symbols + "&apikey=" + API_KEY_EXCHANGE_RATES;
        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
        if (OK.equals(response.getStatusCode())) {
            currencyPairs.forEach(item -> {
                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(response.getBody());
                    if (currencyPairs.size() > 1) {
                        JSONObject data = jsonObject.getJSONObject(item);
                        result.put(item, OBJECT_MAPPER.readValue(data.toString(), ExchangeRatesDto.class));
                    } else {
                        String symbol = (String) jsonObject.get("symbol");
                        BigDecimal rate = (BigDecimal) jsonObject.get("rate");
                        Integer timestamp = (Integer) jsonObject.get("timestamp");
                        result.put(item, new ExchangeRatesDto(symbol, String.valueOf(rate), String.valueOf(timestamp)));
                    }
                } catch (Exception e) {
                    try {
                        jsonObject = new JSONObject(response.getBody());
                        int status = jsonObject.getInt("code");
                        if (HttpStatus.TOO_MANY_REQUESTS.value() == status) {
                            throw new DataCollectorException("Достигнут лимит обращения к API TwelveData");
                        } else {
                            throw new DataCollectorException("Неизвестная ошибка при получении данных с TwelveData");
                        }
                    } catch (JSONException ex) {
                        log.error(ex.getMessage());
                        throw new DataCollectorException("Неизвестная ошибка при получении данных с TwelveData");
                    }
                }
            });
        } else {
            throw new DataCollectorException("Ошибка при сборе данных о рыночном курсе коллектором TwelveData");
        }

        result.forEach((k, v) -> {
            Map<String, String> forCache = (Map<String, String>) this.convertValue(v, Map.class);
            this.jedis.hmset(String.format(EXCHANGE_RATES_REDIS_CACHE_KEY, k), forCache);
            this.jedis.sadd(EXCHANGE_RATES_REDIS_CACHE, v.getSymbol());
        });

        return result;
    }

    private Map<String, TimeSeriesDataDto> getTimeSeriesCache(Collection<String> currencyPairs, String interval) {
        Map<String, TimeSeriesDataDto> cache = new HashMap<>();
        try {
            currencyPairs.forEach(pair -> {
                Map<String, String> map = this.jedis.hgetAll(String.format(QUOTES_REDIS_CACHE_KEY, pair, interval));
                if (map != null && !map.isEmpty()) {
                    TimeSeriesValueDto[] list = JsonUtils.fromJsonString(map.get(pair), TimeSeriesValueDto[].class);
                    if (list != null && list.length > 0) {
                        String datetime = list[0].getDatetime();
                        DateTimeFormatter formatter;
                        if (interval.equalsIgnoreCase("1min")) {
                            formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                            LocalDateTime dateTime = LocalDateTime.parse(datetime, formatter);
                            if (interval.equalsIgnoreCase("1min")
                                    && dateTime.plusMinutes(1).isBefore(LocalDateTime.now())
                                    && !Arrays.asList(7, 1).contains(Calendar.getInstance().get(Calendar.DAY_OF_WEEK))) {
                                throw new OutdatedCacheException("Закончился срок жизни кэша поминутных котировок валюты");
                            }
                        } else {
                            formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                            LocalDate date = LocalDate.parse(datetime, formatter);
                            if (interval.equalsIgnoreCase("1month")
                                    && date.plusDays(1).isBefore(LocalDate.now())
                                    && !Arrays.asList(7, 1).contains(Calendar.getInstance().get(Calendar.DAY_OF_WEEK))) {
                                throw new OutdatedCacheException("Закончился срок жизни кэша ежедневных котировок валюты");
                            }
                        }
                        cache.put(pair, new TimeSeriesDataDto(null, Arrays.asList(list), null));
                    }
                }
            });
        } catch (OutdatedCacheException e) {
            return null;
        }


        //кэш не пустой
        if (!cache.isEmpty()) {
            return cache;
        }
        return null;
    }

    private Map<String, ExchangeRatesDto> getExchangeRatesCache(Collection<String> currencyPairs) {
        Map<String, ExchangeRatesDto> cache = new HashMap<>();
        currencyPairs.forEach(pair -> {
            Map<String, String> map = this.jedis.hgetAll(String.format(EXCHANGE_RATES_REDIS_CACHE_KEY, pair));
            if (map != null && !map.isEmpty()) {
                ExchangeRatesDto dto = (ExchangeRatesDto) this.convertValue(map, ExchangeRatesDto.class);
                cache.put(pair, dto);
            }
        });

        //кэш не пустой и сегодня не выходной день
        if (!cache.isEmpty() && Arrays.asList(7, 1).contains(Calendar.getInstance().get(Calendar.DAY_OF_WEEK))) {
            return cache;
        }
        return null;
    }

    public Object convertValue(Object object, Class clazz) {
        return new ObjectMapper().convertValue(object, clazz);
    }

}
