package ru.team418.financialapp.advices;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ru.team418.financialapp.exceptions.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.ValidationException;
import java.util.HashMap;
import java.util.Map;

/**
 * Кастомный обработчик ошибок системы
 */
@ControllerAdvice
public class ErrorHandlingControllerAdvice extends ResponseEntityExceptionHandler {

    private static final String ERROR_FIELD_NAME = "error";

    @ExceptionHandler(BalanceOperationException.class)
    @ResponseBody
    ResponseEntity<Object> onBalanceOperationExceptionError(BalanceOperationException e, WebRequest request) {
        return handleExceptionInternal(e, Map.of(ERROR_FIELD_NAME, e.getMessage()), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(CreateOrderException.class)
    @ResponseBody
    ResponseEntity<Object> onCreateOrderExceptionError(CreateOrderException e, WebRequest request) {
        return handleExceptionInternal(e, Map.of(ERROR_FIELD_NAME, e.getMessage()), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(DataCollectorException.class)
    @ResponseBody
    ResponseEntity<Object> onDataCollectorExceptionError(DataCollectorException e, WebRequest request) {
        return handleExceptionInternal(e, Map.of(ERROR_FIELD_NAME, e.getMessage()), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

    @ExceptionHandler(OrderProcessingException.class)
    @ResponseBody
    ResponseEntity<Object> onOrderProcessingExceptionError(OrderProcessingException e, WebRequest request) {
        return handleExceptionInternal(e, Map.of(ERROR_FIELD_NAME, e.getMessage()), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

    @ExceptionHandler(UnitUpdateException.class)
    @ResponseBody
    ResponseEntity<Object> onUnitUpdateExceptionError(UnitUpdateException e, WebRequest request) {
        return handleExceptionInternal(e, Map.of(ERROR_FIELD_NAME, e.getMessage()), new HttpHeaders(), HttpStatus.TOO_MANY_REQUESTS, request);
    }

    @ExceptionHandler(ValidationException.class)
    @ResponseBody
    ResponseEntity<Object> onValidationExceptionError(ValidationException e, WebRequest request) {
        return handleExceptionInternal(e, Map.of(ERROR_FIELD_NAME, e.getMessage()), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(DisabledException.class)
    @ResponseBody
    ResponseEntity<Object> onDisabledExceptionError(DisabledException e, WebRequest request) {
        return handleExceptionInternal(e, Map.of(ERROR_FIELD_NAME, "Пользователь заблокирован"), new HttpHeaders(), HttpStatus.FORBIDDEN, request);
    }

    @ExceptionHandler(BadCredentialsException.class)
    @ResponseBody
    ResponseEntity<Object> onBadCredentialsExceptionError(BadCredentialsException e, WebRequest request) {
        return handleExceptionInternal(e, Map.of(ERROR_FIELD_NAME, "Введен неверный логин или пароль"), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseBody
    ResponseEntity<Object> onEntityNotFountException(EntityNotFoundException e, WebRequest request) {
        return handleExceptionInternal(e, Map.of(ERROR_FIELD_NAME, e.getMessage()), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseBody
    ResponseEntity<Object> onIllegalArgumentException(IllegalArgumentException e, WebRequest request) {
        return handleExceptionInternal(e, Map.of(ERROR_FIELD_NAME, e.getMessage()), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(UserExistsException.class)
    @ResponseBody
    ResponseEntity<Object> onUserExistsException(UserExistsException e, WebRequest request) {
        return handleExceptionInternal(e, Map.of(ERROR_FIELD_NAME, e.getMessage()), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(BadRefreshTokenException.class)
    @ResponseBody
    ResponseEntity<Object> onBadRefreshTokenException(BadRefreshTokenException e, WebRequest request) {
        return handleExceptionInternal(e, Map.of(ERROR_FIELD_NAME, e.getMessage()), new HttpHeaders(), HttpStatus.UNAUTHORIZED, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach(error -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return handleExceptionInternal(ex, errors, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
}
