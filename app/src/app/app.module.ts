import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { environment } from "../environments/environment";
import { environmentToken } from "./shared";
import {AuthenticationCoreModule} from "./features/authentication/core/authentication-core.module";
import {NgrxModule} from "./core/store";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgrxModule,
    AuthenticationCoreModule
  ],
  providers: [
    {
      provide: environmentToken,
      useValue: environment,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
