import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {AccountBasicInfo} from "../../types";

@Component({
  selector: 'app-home-actions[mainAccount]',
  templateUrl: './home-actions.component.html',
  styleUrls: ['./home-actions.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeActionsComponent {
  @Input()
  mainAccount!: AccountBasicInfo;
}
