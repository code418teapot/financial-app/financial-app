import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {AccountBasicInfo} from "../../types";
import {currencyIconsMap, currencyTitlesNameMap} from "../../../../shared";
import {ApplicationRoutes} from "../../../../core/routing";
import {AccountRoutes} from "../../../account/routing/routes.enum";

@Component({
  selector: 'app-accounts-list',
  templateUrl: './accounts-list.component.html',
  styleUrls: ['./accounts-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AccountsListComponent {
  @Input()
  accounts: AccountBasicInfo[] = []

  readonly currencyTitlesNameMap = currencyTitlesNameMap;

  readonly currencyIconsMap = currencyIconsMap;

  getAccountRouterLinkOptions(account: AccountBasicInfo): string[] {
    return ['/', ApplicationRoutes.Account, AccountRoutes.Information, account.id.toString()]
  }
}
