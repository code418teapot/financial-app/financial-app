import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {HomeStore} from "./store";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeComponent implements OnInit {
  readonly isLoading$ = this._store.isLoading$;
  readonly mainAccount$ = this._store.mainAccount$;
  readonly accounts$ = this._store.otherAccounts$;

  constructor(private _store: HomeStore) {}

  ngOnInit() {
    this._store.fetchAccounts();
  }
}
