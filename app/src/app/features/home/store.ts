import {Injectable} from "@angular/core";
import {ComponentStore} from "@ngrx/component-store";
import {AccountBasicInfo} from "./types";
import {AccountsDataAccessService} from "../../data-access";
import {catchError, EMPTY, finalize, Observable, switchMap, takeUntil, tap} from "rxjs";

interface HomeStoreState {
  isLoading: boolean;
  accounts: AccountBasicInfo[]
}

const mainCurrency = 'RUB';

@Injectable()
export class HomeStore extends ComponentStore<HomeStoreState>{
  readonly isLoading$ = this.select((state) => state.isLoading);
  readonly mainAccount$ = this.select((state) => {
    return state.accounts.find((account) => account.currency === mainCurrency);
  })
  readonly otherAccounts$ = this.select((state) => state.accounts.filter(
    (account) => account.currency !== mainCurrency
  ));

  constructor(private _accountsDataAccessService: AccountsDataAccessService) {
    super({
      isLoading: true,
      accounts: []
    });
  }

  readonly fetchAccounts = this.effect((trigger$: Observable<void>) => {
    return trigger$.pipe(
      tap(() => this.patchState({ isLoading: true })),
      switchMap(() => this._accountsDataAccessService.getAccounts().pipe(
        finalize(() => this.patchState({ isLoading: false })),
        tap({
          next: (accounts) => this.patchState({
            accounts
          }),
          error: (e) => console.log(e),
        }),
        catchError(() => EMPTY),
        takeUntil(this.destroy$)
      )),
      takeUntil(this.destroy$)
    );
  });
}
