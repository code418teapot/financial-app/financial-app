export interface AccountBasicInfo {
  id: number;
  createdDate: string;
  updatedDate: string;
  currency: string;
  number: string;
  balance: number;
  rubBalance: number;
}
