import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import {RouterModule, Routes} from "@angular/router";
import { AccountsListComponent } from './components/accounts-list/accounts-list.component';
import {MatListModule} from "@angular/material/list";
import {MatIconModule} from "@angular/material/icon";
import {HomeStore} from "./store";
import {DataAccessModule} from "../../data-access";
import {PageModule, VariableModule} from "../../shared";
import { HomeActionsComponent } from './components/home-actions/home-actions.component';
import {AppCurrencyPipeModule} from "../../shared/pipes/app-currency-pipe/app-currency-pipe.module";
import {MatButtonModule} from "@angular/material/button";
import {MatToolbarModule} from "@angular/material/toolbar";

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: HomeComponent,
  }
]
@NgModule({
  declarations: [
    HomeComponent,
    AccountsListComponent,
    HomeActionsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatListModule,
    MatIconModule,
    DataAccessModule,
    VariableModule,
    PageModule,
    AppCurrencyPipeModule,
    MatButtonModule,
    MatToolbarModule
  ],
  providers: [
    HomeStore
  ]
})
export class HomeModule { }
