import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountComponent } from './account.component';
import { RouterModule } from "@angular/router";
import { accountRoutes } from "./routing";
import {AppCurrencyPipeModule} from "../../shared/pipes/app-currency-pipe/app-currency-pipe.module";

@NgModule({
  declarations: [
    AccountComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(accountRoutes),
    AppCurrencyPipeModule
  ]
})
export class AccountModule { }
