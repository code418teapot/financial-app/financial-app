import {AccountBasicInfo} from "../../../home/types";
import {Injectable} from "@angular/core";
import {ComponentStore} from "@ngrx/component-store";
import {catchError, EMPTY, finalize, Observable, switchMap, takeUntil, tap} from "rxjs";
import {AccountsDataAccessService} from "../../../../data-access";
import {Router} from "@angular/router";
import {ApplicationRoutes} from "../../../../core/routing";

interface InsightsStoreState {
  isLoading: boolean;
  accounts: AccountBasicInfo[];
}

@Injectable()
export class InsightsStore extends ComponentStore<InsightsStoreState> {
  readonly isLoading$ = this.select((state) => state.isLoading);
  readonly accounts$ = this.select((state) => state.accounts);

  constructor(
    private _accountsDataAccessService: AccountsDataAccessService,
    private _router: Router,
  ) {
    super({
      accounts: [],
      isLoading: false,
    });

    this.fetchAccounts();
  }

  readonly fetchAccounts = this.effect((trigger$: Observable<void>) => {
    return trigger$.pipe(
      tap(() => this.patchState({ isLoading: true })),
      switchMap(() => this._accountsDataAccessService.getAccounts().pipe(
        finalize(() => this.patchState({ isLoading: false })),
        tap({
          next: (accounts) => this.patchState({
            accounts
          }),
          error: (e) => console.log(e),
        }),
        catchError(() => EMPTY),
        takeUntil(this.destroy$)
      )),
      takeUntil(this.destroy$)
    );
  });

  goBack() {
    this._router.navigate(['/', ApplicationRoutes.Home]);
  }
}
