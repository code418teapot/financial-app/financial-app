import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {ApexChart, ApexNonAxisChartSeries, ApexResponsive, ChartComponent} from 'ng-apexcharts';
import {InsightsStore} from "./store";
import {currencyTitlesNameMap} from "../../../../shared";

export type ChartOptions = {
  series: ApexNonAxisChartSeries;
  chart: ApexChart;
  responsive: ApexResponsive[];
  labels: any;
};

@Component({
  selector: 'app-insights',
  templateUrl: './insights.component.html',
  styleUrls: ['./insights.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InsightsComponent implements OnInit {
  @ViewChild("chart") chart!: ChartComponent;
  public chartOptions: ChartOptions = {
    series: [],
    chart: {
      type: "donut",
    },
    labels: [],
    responsive: [
      {
        breakpoint: 480,
        options: {
          legend: {
            position: "bottom"
          }
        }
      }
    ]
  };

  readonly accounts$ = this._store.accounts$;

  constructor(
    private _store: InsightsStore,
    private _changeDetectorRef: ChangeDetectorRef,
  ) {}

  ngOnInit(): void {
    this.accounts$.subscribe((accounts) => {
      this.chartOptions.series = accounts.map(account => account.rubBalance);
      this.chartOptions.labels = accounts.map(account => currencyTitlesNameMap[account.currency]);

      this._changeDetectorRef.markForCheck();
    })
  }

  handleBackButtonClick() {
    this._store.goBack();
  }
}
