import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InsightsComponent } from './insights.component';
import {RouterModule, Routes} from "@angular/router";
import {PageModule} from "../../../../shared";
import {NgApexchartsModule} from "ng-apexcharts";
import {DataAccessModule} from "../../../../data-access";
import {InsightsStore} from "./store";

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: InsightsComponent
  }
]

@NgModule({
  declarations: [
    InsightsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    PageModule,
    NgApexchartsModule,
    DataAccessModule,
  ],
  providers: [
    InsightsStore
  ]
})
export class InsightsModule { }
