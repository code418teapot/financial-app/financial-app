import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateComponent } from './create.component';
import {RouterModule, Routes} from "@angular/router";
import {PageModule, VariableModule} from "../../../../shared";
import {MatFormFieldModule} from "@angular/material/form-field";
import {ReactiveFormsModule} from "@angular/forms";
import {CreateAccountStore} from "./store";
import {DataAccessModule} from "../../../../data-access";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {MatSelectModule} from "@angular/material/select";

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: CreateComponent
  }
]

@NgModule({
  declarations: [
    CreateComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    PageModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    DataAccessModule,
    VariableModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule
  ],
  providers: [
    CreateAccountStore
  ]
})
export class CreateModule { }
