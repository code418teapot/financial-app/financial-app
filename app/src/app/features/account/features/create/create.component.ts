import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import {CreateAccountStore} from "./store";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateComponent implements OnInit {
  form = new FormGroup({
    currency: new FormControl<string>('RUB', {
      nonNullable: true,
      validators: [
        Validators.required,
      ]
    }),
  });

  readonly hasErrors$ = this._store.hasErrors$;
  readonly isLoading$ = this._store.isLoading$;
  readonly units$ = this._store.units$;

  constructor(private _store: CreateAccountStore) { }

  ngOnInit(): void {
  }

  handleFormSubmit() {
    console.log(this.form.errors);
    if (this.form.invalid) {
      return;
    }

    this._store.createAccount(this.form.getRawValue());
  }


  handleBackButtonClick() {
    this._store.goBack();
  }
}
