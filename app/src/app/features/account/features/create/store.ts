import {Injectable} from "@angular/core";
import {ComponentStore} from "@ngrx/component-store";
import {ActivatedRoute, Router} from "@angular/router";
import {AccountsDataAccessService, UnitsDataAccessService} from "../../../../data-access";
import {catchError, EMPTY, finalize, Observable, switchMap, takeUntil, tap} from "rxjs";
import {CreateAccountFormReturnValue, UnitListItem} from "./types";
import {ApplicationRoutes} from "../../../../core/routing";

interface CreateAccountStoreState {
  isLoading: boolean;
  hasErrors: boolean;
  units: UnitListItem[];
}

@Injectable()
export class CreateAccountStore extends ComponentStore<CreateAccountStoreState> {
  readonly hasErrors$ = this.select((state) => state.hasErrors);
  readonly isLoading$ = this.select((state) => state.isLoading);
  readonly units$ = this.select((state) => state.units);

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _accountsDataAccessService: AccountsDataAccessService,
    private _unitsDataAccessService: UnitsDataAccessService
  ) {
    super({
      hasErrors: false,
      isLoading: false,
      units: [],
    });

    this.fetchUnits();
  }

  readonly fetchUnits = this.effect((trigger$: Observable<void>) => {
    return trigger$.pipe(
      tap(() => this.patchState({ isLoading: true })),
      switchMap(() => this._unitsDataAccessService.getUnits().pipe(
        finalize(() => this.patchState({ isLoading: false })),
        tap({
          next: (units) => this.patchState({
            units
          }),
          error: (e) => console.log(e),
        }),
        catchError(() => EMPTY),
        takeUntil(this.destroy$)
      )),
      takeUntil(this.destroy$)
    );
  });

  readonly createAccount = this.effect((deposit$: Observable<CreateAccountFormReturnValue>) => {
    return deposit$.pipe(
      tap(() => this.patchState({ isLoading: true })),
      switchMap((form) => this._accountsDataAccessService.createAccount(form.currency).pipe(
        finalize(() => this.patchState({ isLoading: false })),
        tap({
          next: () => {
            this._router.navigate(['/', ApplicationRoutes.Home])
          },
          error: (e) => console.log(e),
        }),
        catchError(() => EMPTY),
      )),
    );
  });

  goBack() {
    this._router.navigate(['/', ApplicationRoutes.Home]);
  }
}
