export interface UnitListItem {
  type: string,
  code: string,
  fullName: string,
  symbol: string
}

export interface CreateAccountFormReturnValue {
  currency: string;
}
