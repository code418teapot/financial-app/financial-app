import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import { HistoryComponent } from './history.component';
import {PageModule, VariableModule} from "../../../../shared";
import {MatListModule} from "@angular/material/list";
import {MatIconModule} from "@angular/material/icon";
import {AppCurrencyPipeModule} from "../../../../shared/pipes/app-currency-pipe/app-currency-pipe.module";
import {DataAccessModule} from "../../../../data-access";
import {HistoryStore} from "./store";

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: HistoryComponent,
  }
]
@NgModule({
  declarations: [
    HistoryComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    PageModule,
    MatListModule,
    MatIconModule,
    AppCurrencyPipeModule,
    DataAccessModule,
    VariableModule
  ],
  providers: [
    HistoryStore
  ]
})
export class HistoryModule { }
