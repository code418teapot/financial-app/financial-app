export enum OrderHistoryOperationType {
  Buy= 'BUY',
  Sell = 'SELL',
  Deposit = 'DEPOSIT',
  Withdraw = 'WITHDRAW'
}

export enum OrderHistoryStatus {
  Waiting = 'WAITING',
  Processed = 'Processed',
  Processing = 'Processing',
}

export interface OrderHistoryItem {
  accountId: number;
  amount: number;
  currency: string;
  type: OrderHistoryOperationType,
  dateTime: string;
  status: OrderHistoryStatus
}
