import {OrderHistoryItem} from "./types";
import {Injectable} from "@angular/core";
import {ComponentStore} from "@ngrx/component-store";
import {AccountsDataAccessService} from "../../../../data-access";
import {catchError, EMPTY, finalize, Observable, switchMap, takeUntil, tap} from "rxjs";
import {ApplicationRoutes} from "../../../../core/routing";
import {Router} from "@angular/router";

export interface HistoryStoreState {
  isLoading: boolean;
  orderHistoryItems: OrderHistoryItem[]
}

@Injectable()
export class HistoryStore extends ComponentStore<HistoryStoreState> {
  readonly isLoading$ = this.select((state) => state.isLoading);
  readonly historyItems$ = this.select((state) => state.orderHistoryItems);

  constructor(
    private _accountsDataAccessService: AccountsDataAccessService,
    private _router: Router,
  ) {
    super({
      isLoading: true,
      orderHistoryItems: []
    });
  }

  readonly fetchOrderHistoryItems = this.effect((trigger$: Observable<void>) => {
    return trigger$.pipe(
      tap(() => this.patchState({ isLoading: true })),
      switchMap(() => this._accountsDataAccessService.getHistory().pipe(
        finalize(() => this.patchState({ isLoading: false })),
        tap({
          next: (items) => this.patchState({
            orderHistoryItems: items.map(item => ({
              amount: item.amount,
              currency: item.currency,
              type: item.type,
              status: item.status
            } as OrderHistoryItem))
          }),
          error: (e) => console.log(e),
        }),
        catchError(() => EMPTY),
        takeUntil(this.destroy$)
      )),
      takeUntil(this.destroy$)
    );
  });

  goBack() {
    this._router.navigate(['/', ApplicationRoutes.Home]);
  }
}
