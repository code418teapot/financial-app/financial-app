import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {currencyIconsMap, currencyTitlesNameMap} from 'src/app/shared';
import {OrderHistoryOperationType} from "./types";
import {HistoryStore} from "./store";

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HistoryComponent implements OnInit {
  readonly items$ = this._store.historyItems$;

  readonly currencyTitlesNameMap = currencyTitlesNameMap;

  readonly currencyIconsMap = currencyIconsMap;

  readonly OrderHistoryOperationType = OrderHistoryOperationType;

  constructor(private _store: HistoryStore) {
  }

  coerceOperationTypeToString(type: OrderHistoryOperationType): string {
    switch (type) {
      case OrderHistoryOperationType.Buy: return 'Покупка';
      case OrderHistoryOperationType.Sell: return 'Продажа';
      case OrderHistoryOperationType.Deposit: return 'Пополнение';
      case OrderHistoryOperationType.Withdraw: return 'Вывод средств';
    }
  }

  coerceOperationTypeToAmountSign(type: OrderHistoryOperationType): string {
    if (type === OrderHistoryOperationType.Buy || type === OrderHistoryOperationType.Withdraw) {
      return '-'
    }

    return '+';
  }

  handleBackButtonClick() {
    this._store.goBack();
  }

  ngOnInit() {
    this._store.fetchOrderHistoryItems();
  }
}
