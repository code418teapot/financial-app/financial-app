import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WithdrawComponent } from './withdraw.component';
import {RouterModule, Routes} from "@angular/router";
import {WithdrawStore} from "./store";
import {PageModule, VariableModule} from "../../../../shared";
import {AccountBalanceInfoModule} from "../../../../shared/components/account-balance-info";
import {MatFormFieldModule} from "@angular/material/form-field";
import {ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: WithdrawComponent
  }
]

@NgModule({
  declarations: [
    WithdrawComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    VariableModule,
    PageModule,
    AccountBalanceInfoModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule
  ],
  providers: [
    WithdrawStore
  ]
})
export class WithdrawModule { }
