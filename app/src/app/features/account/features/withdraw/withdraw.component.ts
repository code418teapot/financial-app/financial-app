import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {WithdrawStore} from "./store";

@Component({
  selector: 'app-withdraw',
  templateUrl: './withdraw.component.html',
  styleUrls: ['./withdraw.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WithdrawComponent implements OnInit {
  form = new FormGroup({
    amount: new FormControl<number>(0, {
      nonNullable: true,
      validators: [
        Validators.required,
      ]
    }),
  });

  readonly hasErrors$ = this._store.hasErrors$;
  readonly isLoading$ = this._store.isLoading$;
  readonly account$ = this._store.account$;

  constructor(private _store: WithdrawStore) { }

  ngOnInit(): void {
    this.account$.subscribe((account) => {
      console.log(account?.balance);
      this.form.controls.amount.setValidators([
        Validators.required,
        Validators.max(account?.balance ?? 0)
      ]);
      this.form.controls.amount.updateValueAndValidity();
    })
  }

  handleFormSubmit() {
    if (this.form.invalid) {
      console.log(this.form.controls.amount.errors);
      return;
    }

    this._store.withdraw(this.form.getRawValue())
  }

  handleBackButtonClick() {
    this._store.goBack();
  }
}
