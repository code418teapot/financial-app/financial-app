export interface WithdrawFormReturnValue {
  amount: number;
}
