import {AccountBasicInfo} from "../../../home/types";
import {Injectable} from "@angular/core";
import {ComponentStore} from "@ngrx/component-store";
import {ActivatedRoute, Router} from "@angular/router";
import {AccountsDataAccessService} from "../../../../data-access";
import {
  catchError,
  combineLatest,
  EMPTY,
  delay,
  filter,
  finalize,
  map,
  Observable,
  switchMap,
  takeUntil,
  tap,
} from "rxjs";
import {isNil} from "lodash";
import {ApplicationRoutes} from "../../../../core/routing";
import {
  currencyQueryParameterName,
  orderOperationTypeQueryParameterName,
  recieverIdQueryParameterName,
  senderIdQueryParameterName
} from "../../types";
import {OrderFormReturnValue} from "./types";

export interface OrderStoreState {
  isLoading: boolean;
  hasErrors: boolean;
  mainAccount?: AccountBasicInfo;
}

@Injectable()
export class OrderStore extends ComponentStore<OrderStoreState> {
  readonly hasErrors$ = this.select((state) => state.hasErrors);
  readonly isLoading$ = this.select((state) => state.isLoading);
  readonly mainAccount$ = this.select((state) => state.mainAccount);

  readonly senderIdQueryParameter$ = this._activatedRoute.queryParams.pipe(
    map((queryParams) => {
      return queryParams[senderIdQueryParameterName] as number | undefined;
    }),
    filter(m => !isNil(m)),
    takeUntil(this.destroy$),
  );

  readonly recieverIdQueryParameter$ = this._activatedRoute.queryParams.pipe(
    map((queryParams) => {
      return queryParams[recieverIdQueryParameterName] as number | undefined;
    }),
    filter(m => !isNil(m)),
    takeUntil(this.destroy$),
  );

  readonly operationTypeQueryParameter$ = this._activatedRoute.queryParams.pipe(
    map((queryParams) => {
      return queryParams[orderOperationTypeQueryParameterName] as string | undefined;
    }),
    filter(m => !isNil(m)),
    takeUntil(this.destroy$),
  );

  readonly currencyQueryParameter$ = this._activatedRoute.queryParams.pipe(
    map((queryParams) => {
      return queryParams[currencyQueryParameterName] as string | undefined;
    }),
    filter(m => !isNil(m)),
    takeUntil(this.destroy$),
  );

  readonly title$ = this.operationTypeQueryParameter$.pipe(
    map(operationType => operationType === 'buy' ? 'Покупка' : 'Продажа'),
    takeUntil(this.destroy$)
  );

  readonly buttonTitle$ = this.operationTypeQueryParameter$.pipe(
    map(operationType => operationType === 'buy' ? 'Купить' : 'Продать'),
    takeUntil(this.destroy$)
  );

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _accountsDataAccessService: AccountsDataAccessService,
  ) {
    super({
      isLoading: false,
      hasErrors: false,
    });
  }

  readonly fetchMainAccountInfo = this.effect((trigger$: Observable<void>) => {
    return trigger$.pipe(
      tap(() => this.patchState({ isLoading: true })),
    switchMap(() => this.operationTypeQueryParameter$.pipe(takeUntil(this.destroy$))),
      switchMap((operationType) => {
        if (operationType === 'buy') {
          return this.recieverIdQueryParameter$;
        }
        else {
          return this.senderIdQueryParameter$;
        }
      }),
      switchMap((mainAccountId) => this._accountsDataAccessService.getAccount(mainAccountId!).pipe(
        finalize(() => this.patchState({ isLoading: false })),
        tap({
          next: (account) => this.patchState({
            mainAccount: account,
          }),
          error: (e) => console.log(e),
        }),
        catchError(() => EMPTY),
        takeUntil(this.destroy$)
      )),
      takeUntil(this.destroy$)
    );
  });

  readonly makeOrder = this.effect((makeOrder$: Observable<OrderFormReturnValue>) => {
    return combineLatest([
      makeOrder$,
      this.senderIdQueryParameter$,
      this.recieverIdQueryParameter$,
      this.operationTypeQueryParameter$,
      this.currencyQueryParameter$
    ]).pipe(
      tap(() => this.patchState({ isLoading: true })),
      switchMap(([form, senderId, recieverId, type, currency]) => this._accountsDataAccessService.order({
        currency: currency!,
        amount: form.amount,
        accountSenderId: senderId!,
        accountReceiverId: recieverId!,
      }, type === 'sell' ? 'sell' : 'buy') .pipe(
        delay(5000),
        finalize(() => this.patchState({ isLoading: false })),
        tap({
          next: () => {
            this._router.navigate(['/', ApplicationRoutes.Home])
          },
          error: (e) => console.log(e),
        }),
        catchError(() => EMPTY),
      )),
    );
  });

  goBack() {
    this._router.navigate(['/', ApplicationRoutes.Home]);
  }
}
