import { ChangeDetectionStrategy, Component } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {OrderStore} from "./store";

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    OrderStore
  ]
})
export class OrderComponent {
  form = new FormGroup({
    amount: new FormControl<number>(0, {
      nonNullable: true,
      validators: [
        Validators.required,
        Validators.min(1),
        Validators.pattern('^\\d+$'),
      ]
    }),
  });

  readonly hasErrors$ = this._store.hasErrors$;
  readonly isLoading$ = this._store.isLoading$;
  readonly account$ = this._store.mainAccount$;
  readonly title$ = this._store.title$;
  readonly buttonTitle$ = this._store.buttonTitle$;

  constructor(private _store: OrderStore) {
  }

  ngOnInit(): void {
    this._store.fetchMainAccountInfo();
  }

  handleFormSubmit() {
    if (this.form.invalid) {
      return;
    }

    this._store.makeOrder(this.form.getRawValue());
  }

  handleBackButtonClick() {
    this._store.goBack();
  }

  ngOnDestroy() {
    this._store.ngOnDestroy();
  }
}
