import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderComponent } from './order.component';
import {RouterModule, Routes} from "@angular/router";
import {PageModule, VariableModule} from "../../../../shared";
import {AccountBalanceInfoModule} from "../../../../shared/components/account-balance-info";
import {MatFormFieldModule} from "@angular/material/form-field";
import {DataAccessModule} from "../../../../data-access";
import {ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";

const routes: Routes = [
  {
    path: '',
    pathMatch: "full",
    component: OrderComponent,
  }
]

@NgModule({
  declarations: [
    OrderComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    VariableModule,
    PageModule,
    AccountBalanceInfoModule,
    MatFormFieldModule,
    DataAccessModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
  ]
})
export class OrderModule { }
