import { ChangeDetectionStrategy, Component } from '@angular/core';
import { currencyTitlesNameMap } from '../../../../shared';
import {AccountInfoStore} from "./store";

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InformationComponent {
  readonly account$ = this._store.account$;
  readonly isLoading$ = this._store.isLoading$;
  readonly exchangeRate$ = this._store.exchangeRate$;
  readonly chartData$ = this._store.chartData$;

  readonly currencyTitlesNameMap = currencyTitlesNameMap;

  constructor(private _store: AccountInfoStore) {

  }

  ngOnInit() {
    this._store.fetchAccountInformation();
  }

  handleBackButtonClick() {
    this._store.goBack();
  }

  sell(): void {
    this._store.sell();
  }

  buy(): void {
    this._store.buy();
  }
}
