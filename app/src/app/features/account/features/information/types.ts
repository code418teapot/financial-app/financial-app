export enum ChartViewType {
  Chart,
  Candles
}

export enum ChartViewInterval {
  Month = 'month',
  Day = 'day',
  Hour = 'hour',
  Minutes = 'minutes',
}

export interface Quote {
  datetime: string,
  open: number,
  high: number,
  low: number,
  close: number
}
