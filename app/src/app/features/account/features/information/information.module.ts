import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InformationComponent } from './information.component';
import {RouterModule, Routes} from "@angular/router";
import {PageModule, VariableModule} from "../../../../shared";
import {AccountInfoStore} from "./store";
import {DataAccessModule} from "../../../../data-access";
import {AppCurrencyPipeModule} from "../../../../shared/pipes/app-currency-pipe/app-currency-pipe.module";
import { CandlestickChartComponent } from './components/candlestick-chart/candlestick-chart.component';
import {NgApexchartsModule} from "ng-apexcharts";
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {MatButtonModule} from "@angular/material/button";
import {MatMenuModule} from "@angular/material/menu";
import {MatIconModule} from "@angular/material/icon";

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: InformationComponent
  }
]

@NgModule({
  declarations: [
    InformationComponent,
    CandlestickChartComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    PageModule,
    DataAccessModule,
    VariableModule,
    AppCurrencyPipeModule,
    NgApexchartsModule,
    MatButtonToggleModule,
    MatButtonModule,
    MatMenuModule,
    MatIconModule
  ],
  providers: [
    AccountInfoStore
  ]
})
export class InformationModule { }
