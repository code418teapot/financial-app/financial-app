import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {
  ApexAxisChartSeries,
  ApexChart,
  ApexTitleSubtitle,
  ApexTooltip,
  ApexXAxis,
  ApexYAxis,
  ChartComponent
} from 'ng-apexcharts';
import * as dayjs from "dayjs";
import {Quote} from "../../types";

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  yaxis: ApexYAxis;
  title: ApexTitleSubtitle;
  tooltip: ApexTooltip;
};

@Component({
  selector: 'app-candlestick-chart',
  templateUrl: './candlestick-chart.component.html',
  styleUrls: ['./candlestick-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CandlestickChartComponent implements OnChanges {
  @Input()
  data: Quote[] = []

  @ViewChild("chart")
  chart!: ChartComponent;

  public chartOptions: ChartOptions;

  constructor(
    private _changeDetectorRef: ChangeDetectorRef
  ) {
    this.chartOptions = this._generateChart();
  }


  ngOnChanges(changes: SimpleChanges): void {
    const dataChanges = changes['data'];
    console.log(dataChanges);

    if (dataChanges.currentValue !== dataChanges.previousValue) {
      this.chartOptions = this._generateChart();
      this._changeDetectorRef.markForCheck();
    }
  }

  private _generateChart(): ChartOptions {
    return {
      series: [
        {
          name: "candle",
          data: this.data.map((quote) => ({
            x: quote.datetime,
            y: [quote.open, quote.high, quote.low, quote.close]
          }))
        }
      ],
      chart: {
        height: 256,
        type: "candlestick",
        toolbar: {
          show: false
        }
      },
      title: {
        text: "",
        align: "left",
      },
      tooltip: {
        enabled: true
      },
      xaxis: {
        type: "category",
        labels: {
          formatter: function (val) {
            return dayjs(val).format("MMM DD HH:mm");
          }
        }
      },
      yaxis: {
        // max: Math.max(...this.data.map(q => q.high)),
        // min: Math.min(...this.data.map(q => q.low)),
        tooltip: {
          enabled: true
        }
      }
    }
  }
}
