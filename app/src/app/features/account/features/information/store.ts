import {ChartViewInterval, ChartViewType, Quote} from "./types";
import {Injectable} from "@angular/core";
import {ComponentStore} from "@ngrx/component-store";
import {AccountsDataAccessService, UnitsDataAccessService} from "../../../../data-access";
import {ActivatedRoute, Router} from "@angular/router";
import {catchError, EMPTY, filter, finalize, map, Observable, switchMap, takeUntil, tap} from "rxjs";
import {AccountBasicInfo} from "../../../home/types";
import {isNil} from "lodash";
import {ApplicationRoutes} from "../../../../core/routing";
import {AccountRoutes} from "../../routing/routes.enum";
import {
  currencyQueryParameterName,
  orderOperationTypeQueryParameterName,
  recieverIdQueryParameterName,
  senderIdQueryParameterName
} from "../../types";

const accountIdParameterName = 'accountId';

interface AccountInfoStoreState {
  isLoading: boolean;
  account?: AccountBasicInfo;
  mainAccount?: AccountBasicInfo;
  chartType: ChartViewType;
  chartViewInterval: ChartViewInterval;
  chartData: Quote[],
  exchangeRate: number;
}

@Injectable()
export class AccountInfoStore extends ComponentStore<AccountInfoStoreState> {
  readonly isLoading$ = this.select((state) => state.isLoading);
  readonly account$ = this.select((state) => state.account);

  readonly exchangeRate$ = this.select(state => state.exchangeRate);
  readonly chartData$ = this.select((state) => state.chartData);

  readonly accountIdParameter$ = this._activatedRoute.queryParams.pipe(
    map((params) => {
      console.log(params);
      return params[accountIdParameterName] as number | undefined;
    }),
    filter(accountId => !isNil(accountId)),
    takeUntil(this.destroy$),
  );

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _accountsDataAccessService: AccountsDataAccessService,
    private _unitsDataAccessService: UnitsDataAccessService,
  ) {
    super({
      isLoading: false,
      chartType: ChartViewType.Chart,
      chartViewInterval: ChartViewInterval.Minutes,
      chartData: [],
      exchangeRate: 0,
    });

    this._getMainAccount()
      .subscribe(account => {
        this.patchState({
          mainAccount: account
        })
      });

    this.account$.pipe(
      filter((account) => !isNil(account)),
      takeUntil(this.destroy$),
      switchMap(account => {
        return this._unitsDataAccessService.getUnitsInformation(account!.currency);
      })
    )
      .subscribe(quotes => {
        this.patchState({
          chartData: quotes.minutelyQuoteData,
          exchangeRate: quotes.exchangeRate,
        })
      })
  }

  readonly fetchAccountInformation = this.effect((trigger$: Observable<void>) => {
    return trigger$.pipe(
      tap(() => this.patchState({isLoading: true})),
      switchMap(() => this.accountIdParameter$),
      tap(console.log),
      switchMap((accountId) => this._accountsDataAccessService.getAccount(accountId!).pipe(
        finalize(() => this.patchState({isLoading: false})),
        tap({
          next: (account) => this.patchState({
            account
          }),
          error: (e) => console.log(e),
        }),
        catchError(() => EMPTY),
        takeUntil(this.destroy$)
      )),
      takeUntil(this.destroy$)
    );
  });

  goBack() {
    this._router.navigate(['/', ApplicationRoutes.Home]);
  }

  private _getMainAccount(): Observable<AccountBasicInfo> {
    return this._accountsDataAccessService.getAccounts()
      .pipe(
        map(accounts => {
          return accounts.find(account => account.currency === 'RUB')
        }),
        filter(account => account !== null && account !== undefined),
        map(account => account!),
        takeUntil(this.destroy$)
      );
  }

  buy(): void {
    this._router.navigate([
      '/',
      ApplicationRoutes.Account,
      AccountRoutes.Order
    ], {
      queryParams: {
        [senderIdQueryParameterName]: this.get(s => s.mainAccount!.id),
        [recieverIdQueryParameterName]: this.get(s => s.account!.id),
        [currencyQueryParameterName]: this.get(s => s.account!.currency),
        [orderOperationTypeQueryParameterName]: 'buy'
      }
    })
  }

  sell(): void {
    this._router.navigate([
      '/',
      ApplicationRoutes.Account,
      AccountRoutes.Order
    ], {
      queryParams: {
        [senderIdQueryParameterName]: this.get(s => s.account!.id),
        [recieverIdQueryParameterName]: this.get(s => s.mainAccount!.id),
        [currencyQueryParameterName]: this.get(s => s.account!.currency),
        [orderOperationTypeQueryParameterName]: 'sell'
      }
    });
  }
}
