export interface DepositFormReturnValue {
  amount: number;
}
