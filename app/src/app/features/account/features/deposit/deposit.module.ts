import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DepositComponent } from './deposit.component';
import {PageModule, VariableModule} from "../../../../shared";
import {RouterModule, Routes} from "@angular/router";
import {DepositStore} from "./store";
import {MatFormFieldModule} from "@angular/material/form-field";
import {ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {AccountBalanceInfoModule} from "../../../../shared/components/account-balance-info";

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: DepositComponent
  }
]

@NgModule({
  declarations: [
    DepositComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    PageModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    VariableModule,
    MatButtonModule,
    AccountBalanceInfoModule,
  ],
  providers: [
    DepositStore
  ]
})
export class DepositModule { }
