import { ChangeDetectionStrategy, Component } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {DepositStore} from "./store";

@Component({
  selector: 'app-deposit',
  templateUrl: './deposit.component.html',
  styleUrls: ['./deposit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DepositComponent {
  form = new FormGroup({
    amount: new FormControl<number>(0, {
      nonNullable: true,
      validators: [
        Validators.required,
        Validators.min(0.01),
      ]
    }),
  });

  readonly hasErrors$ = this._store.hasErrors$;
  readonly isLoading$ = this._store.isLoading$;
  readonly account$ = this._store.account$;

  constructor(private _store: DepositStore) {
  }

  handleFormSubmit() {
    if (this.form.invalid) {
      return;
    }

    this._store.deposit(this.form.getRawValue())
  }

  handleBackButtonClick() {
    this._store.goBack();
  }
}
