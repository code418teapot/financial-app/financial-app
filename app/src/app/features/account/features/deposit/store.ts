import {Injectable} from "@angular/core";
import {ComponentStore} from "@ngrx/component-store";
import {ActivatedRoute, Router} from "@angular/router";
import {catchError, delay, EMPTY, finalize, map, Observable, switchMap, takeUntil, tap} from "rxjs";
import {accountIdQueryParameterName} from "../../types";
import {ApplicationRoutes} from "../../../../core/routing";
import {AccountsDataAccessService} from "../../../../data-access";
import {AccountBasicInfo} from "../../../home/types";
import {DepositFormReturnValue} from "./types";

interface DepositStoreState {
  isLoading: boolean;
  hasErrors: boolean;
  account?: AccountBasicInfo;
}

@Injectable()
export class DepositStore extends ComponentStore<DepositStoreState> {
  readonly hasErrors$ = this.select((state) => state.hasErrors);
  readonly isLoading$ = this.select((state) => state.isLoading);
  readonly account$ = this.select((state) => state.account);

  readonly accountIdQueryParameter$ = this._activatedRoute.queryParams.pipe(
    map((queryParams) => {
      return queryParams[accountIdQueryParameterName] as number | undefined;
    }),
    takeUntil(this.destroy$),
  );

  constructor(
      private _activatedRoute: ActivatedRoute,
      private _router: Router,
      private _accountsDataAccessService: AccountsDataAccessService,
  ) {
      super({
        hasErrors: false,
        isLoading: false
      });

    this.fetchAccountInformation();
  }

  readonly fetchAccountInformation = this.effect((trigger$: Observable<void>) => {
    return trigger$.pipe(
      tap(() => this.patchState({ isLoading: true })),
      switchMap(() => this.accountIdQueryParameter$),
      tap(console.log),
      switchMap((accountId) => this._accountsDataAccessService.getAccount(accountId!).pipe(
        finalize(() => this.patchState({ isLoading: false })),
        tap({
          next: (account) => this.patchState({
            account
          }),
          error: (e) => console.log(e),
        }),
        catchError(() => EMPTY),
        takeUntil(this.destroy$)
      )),
      takeUntil(this.destroy$)
    );
  });

  readonly deposit = this.effect((deposit$: Observable<DepositFormReturnValue>) => {
    return deposit$.pipe(
      tap(() => this.patchState({ isLoading: true })),
      switchMap((form) => this._accountsDataAccessService.deposit({
        currency: 'RUB',
        accountReceiverId: this.get(state => state.account?.id)!,
        amount: form.amount
      }).pipe(
        delay(5000),
        finalize(() => this.patchState({ isLoading: false })),
        tap({
          next: () => {
            this._router.navigate(['/', ApplicationRoutes.Home])
          },
          error: (e) => console.log(e),
        }),
        catchError(() => EMPTY),
      )),
    );
  });

  goBack() {
    this._router.navigate(['/', ApplicationRoutes.Home]);
  }
}
