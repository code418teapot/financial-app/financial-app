export enum AccountRoutes {
  Create = 'create',
  Information = 'info',
  Withdraw = 'withdraw',
  Deposit = 'deposit',
  Order = 'order',
  History = 'history',
  Insights = 'insights'
}
