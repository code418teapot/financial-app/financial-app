import { Routes } from "@angular/router";
import { AccountRoutes } from "./routes.enum";
import { AccountComponent } from "../account.component";

export const accountRoutes: Routes = [
  {
    path: '',
    component: AccountComponent,
    children: [
      {
        path: AccountRoutes.Create,
        loadChildren: async () =>
          (await import('../features/create')).CreateModule,
      },
      {
        path: AccountRoutes.Information,
        loadChildren: async () =>
          (await import('../features/information')).InformationModule,
      },
      {
        path: AccountRoutes.Withdraw,
        loadChildren: async () =>
          (await import('../features/withdraw')).WithdrawModule,
      },
      {
        path: AccountRoutes.Deposit,
        loadChildren: async () =>
          (await import('../features/deposit')).DepositModule,
      },
      {
        path: AccountRoutes.History,
        loadChildren: async () =>
          (await import('../features/history')).HistoryModule,
      },
      {
        path: AccountRoutes.Insights,
        loadChildren: async () =>
          (await import('../features/insights')).InsightsModule,
      },
      {
        path: AccountRoutes.Order,
        loadChildren: async () =>
          (await import('../features/order')).OrderModule,
        data: {
          title: 'Продажа'
        }
      }
    ],
  },
];
