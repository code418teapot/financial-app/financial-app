import { Routes } from "@angular/router";
import {AdminComponent} from "../admin.component";
import {AdminRoutes} from "./routes.enum";

export const adminRoutes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: AdminRoutes.Users,
      },
      {
        path: AdminRoutes.Users,
        loadChildren: async () =>
          (await import('../features/users')).UsersModule,
      },
      {
        path: AdminRoutes.EditUser,
        loadChildren: async () =>
          (await import('../features/edit-user')).EditUserModule,
      },
    ],
  },
];
