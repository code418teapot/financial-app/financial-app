import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import {UsersStore} from "./store";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UsersComponent implements OnInit {
  readonly activeUsers$ = this._store.activeUsers$;
  readonly inactiveUsers$ = this._store.inactiveUsers$;

  constructor(private _store: UsersStore) { }

  ngOnInit(): void {
    this._store.fetchUsers();
  }

}
