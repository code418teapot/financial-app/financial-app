export interface UserModel {
  id: number,
  firstName: string,
  lastName: string,
  middleName?: string,
  email: string,
  phoneNumber: string,
  active: true,
  roles: string[]
}
