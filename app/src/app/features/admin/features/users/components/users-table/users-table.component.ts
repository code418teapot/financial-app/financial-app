import {ChangeDetectionStrategy, Component, Input } from '@angular/core';
import {UserModel} from "../../types";
import {UsersStore} from "../../store";

@Component({
  selector: 'app-users-table',
  templateUrl: './users-table.component.html',
  styleUrls: ['./users-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UsersTableComponent {
  @Input()
  users: UserModel[] = [];

  displayedColumns: string[] = ['id', 'name', 'status'];

  coerceUserModelToStatus(user: UserModel): string {
    if (user.active) return 'Активен';

    if (user.roles.includes("NON_CONFIRMED")) {
      return 'Ожидает подтверждения';
    }

    return 'Заблокирован';
  }

  constructor(
    private _store: UsersStore
  ) {
  }

  handleRowClick(row: UserModel) {
    this._store.openUserPage(row);
  }
}
