import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './users.component';
import {RouterModule, Routes} from "@angular/router";
import {PageModule, VariableModule} from "../../../../shared";
import {MatTabsModule} from "@angular/material/tabs";
import { UsersTableComponent } from './components/users-table/users-table.component';
import {MatTableModule} from "@angular/material/table";
import {DataAccessModule} from "../../../../data-access";
import {UsersStore} from "./store";

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: UsersComponent
  },
]
@NgModule({
  declarations: [
    UsersComponent,
    UsersTableComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    PageModule,
    MatTabsModule,
    MatTableModule,
    DataAccessModule,
    VariableModule
  ],
  providers: [
    UsersStore
  ]
})
export class UsersModule { }
