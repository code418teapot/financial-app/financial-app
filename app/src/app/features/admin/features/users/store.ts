import {UserModel} from "./types";
import {Injectable} from "@angular/core";
import {ComponentStore} from "@ngrx/component-store";
import {AdminDataAccessService} from "../../../../data-access";
import {catchError, EMPTY, finalize, Observable, switchMap, takeUntil, tap} from "rxjs";
import {Router} from "@angular/router";
import {ApplicationRoutes} from "../../../../core/routing";
import {AdminRoutes} from "../../routing/routes.enum";
import {userIdQueryParameterName} from "../edit-user/store";

export interface UsersStoreState {
  isLoading: boolean;
  users: UserModel[]
}

@Injectable()
export class UsersStore extends ComponentStore<UsersStoreState> {
  readonly activeUsers$ = this.select(state => state.users.filter(u => !u.roles.includes("NON_CONFIRMED")));
  readonly inactiveUsers$ = this.select(state => state.users.filter(u => u.roles.includes("NON_CONFIRMED")));

  constructor(
    private _adminDataAccessService: AdminDataAccessService,
    private _router: Router,
  ) {
    super({
      isLoading: false,
      users: []
    });
  }

  readonly fetchUsers = this.effect((trigger$: Observable<void>) => {
    return trigger$.pipe(
      tap(() => this.patchState({ isLoading: true })),
      switchMap(() => this._adminDataAccessService.getUsers().pipe(
        finalize(() => this.patchState({ isLoading: false })),
        tap({
          next: (users) => this.patchState({
            users
          }),
          error: (e) => console.log(e),
        }),
        catchError(() => EMPTY),
        takeUntil(this.destroy$)
      )),
      takeUntil(this.destroy$)
    );
  });

  openUserPage(row: UserModel) {
    this._router.navigate([
      '/', ApplicationRoutes.Admin, AdminRoutes.EditUser
    ], {
      queryParams: {
        [userIdQueryParameterName]: row.id
      }
    })
  }
}
