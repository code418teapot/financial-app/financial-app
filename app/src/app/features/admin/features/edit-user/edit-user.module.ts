import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditUserComponent } from './edit-user.component';
import {RouterModule, Routes} from "@angular/router";
import {PageModule, VariableModule} from "../../../../shared";
import {EditUserStore} from "./store";
import {MatButtonModule} from "@angular/material/button";

const routes: Routes = [
  {
    path: '',
    pathMatch: "full",
    component: EditUserComponent,
  }
]

@NgModule({
  declarations: [
    EditUserComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    PageModule,
    VariableModule,
    MatButtonModule,
  ],
  providers:[
    EditUserStore
  ]
})
export class EditUserModule { }
