import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import {EditUserStore} from "./store";
import {UserModel} from "../users/types";

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditUserComponent implements OnInit {
  readonly hasErrors$ = this._store.hasErrors$;
  readonly isLoading$ = this._store.isLoading$;
  readonly user$ = this._store.user$;
  constructor(private _store: EditUserStore) { }

  ngOnInit(): void {
    this._store.fetchUserInfo();
  }

  handleBackButtonClick() {
    this._store.goBack();
  }

  coerceUserModelToStatus(user: UserModel): string {
    if (user.active) return 'Активен';

    if (user.roles.includes("NON_CONFIRMED")) {
      return 'Ожидает подтверждения';
    }

    return 'Заблокирован';
  }

  activateUser(id: number) {
    this._store.activateUser(id);
  }
}
