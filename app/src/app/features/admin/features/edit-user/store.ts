import {UserModel} from "../users/types";
import {Injectable} from "@angular/core";
import {ComponentStore} from "@ngrx/component-store";
import {catchError, EMPTY, finalize, map, Observable, switchMap, takeUntil, tap} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {AdminDataAccessService} from "../../../../data-access";
import {ApplicationRoutes} from "../../../../core/routing";

export const userIdQueryParameterName = 'userId';

interface EditUserStoreState {
  isLoading: boolean;
  hasErrors: boolean;
  user?: UserModel;
}

@Injectable()
export class EditUserStore extends ComponentStore<EditUserStoreState> {
  readonly hasErrors$ = this.select((state) => state.hasErrors);
  readonly isLoading$ = this.select((state) => state.isLoading);
  readonly user$ = this.select((state) => state.user);

  readonly userIdQueryParameter$ = this._activatedRoute.queryParams.pipe(
    map((queryParams) => {
      return queryParams[userIdQueryParameterName] as number | undefined;
    }),
    takeUntil(this.destroy$),
  );

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _adminDataAccessService: AdminDataAccessService,
  ) {
    super({
      hasErrors: false,
      isLoading: false
    });
  }

  readonly fetchUserInfo = this.effect((trigger$: Observable<void>) => {
    return trigger$.pipe(
      tap(() => this.patchState({ isLoading: true })),
      switchMap(() => this.userIdQueryParameter$),
      tap(console.log),
      switchMap((userId) => this._adminDataAccessService.getUserById(userId!).pipe(
        finalize(() => this.patchState({ isLoading: false })),
        tap({
          next: (user) => this.patchState({
            user
          }),
          error: (e) => console.log(e),
        }),
        catchError(() => EMPTY),
        takeUntil(this.destroy$)
      )),
      takeUntil(this.destroy$)
    );
  });

  readonly activateUser = this.effect((userId$: Observable<number>) => {
    return userId$.pipe(
      tap(() => this.patchState({ isLoading: true })),
      switchMap((userId) => this._adminDataAccessService.activateUser(userId).pipe(
        finalize(() => this.patchState({ isLoading: false })),
        tap({
          next: () => {
            this._router.navigate(['/', ApplicationRoutes.Admin])
          },
          error: (e) => console.log(e),
        }),
        catchError(() => EMPTY),
      )),
    );
  });

  goBack() {
    this._router.navigate(['/', ApplicationRoutes.Admin])
  }
}
