export interface CreateProfileFormValue {
  lastName: string;
  firstName: string;
  middleName?: string | null;
  email: string;
  phone: string
  passportSeries: string
  passportNumber: string
  password: string
  confirmPassword: string
}
