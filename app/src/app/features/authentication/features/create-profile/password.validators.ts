import {
  AbstractControl,
  ValidationErrors,
  ValidatorFn
} from '@angular/forms';

export function passwordLengthValidator({
                                          minLength,
                                          maxLength
                                        }: {
  minLength: number;
  maxLength: number;
}): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const controlValue = control.value as string;

    if (controlValue.length < minLength || controlValue.length > maxLength) {
      return {
        passwordLength: {
          value: controlValue.length,
          length: controlValue.length
        }
      };
    }

    return null;
  };
}

/**
 * Checks that control value contains at least one character(A-Z, a-z)
 * @returns ValidatorFn
 */
export function valueContainsCharacterValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const controlValue = control.value as string;

    if (!/[A-Za-z]+/.test(controlValue)) {
      return {
        passwordContainsCharacter: {
          value: controlValue
        }
      };
    }

    return null;
  };
}

/**
 * Checks that control value contains at least one digit(0-9)
 * @returns ValidatorFn
 */
export function valueContainsDigitValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const controlValue = control.value as string;

    if (!/\d+/.test(controlValue)) {
      return {
        passwordContainsDigit: {
          value: controlValue
        }
      };
    }

    return null;
  };
}

/**
 * Checks that control value contains at least one special character
 * @returns ValidatorFn
 */
export function valueContainsSpecialCharacterValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const controlValue = control.value as string;

    if (!/[!#$%&*<>?@^~-]+/.test(controlValue)) {
      return {
        passwordContainsSpecialCharacter: {
          value: controlValue
        }
      };
    }

    return null;
  };
}

export function controlValuesMatchValidator(
  getOriginalControl: () => AbstractControl | null
): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    if (!getOriginalControl()) {
      return null;
    }

    const originalControl = getOriginalControl();

    if (!originalControl) {
      return null;
    }

    const controlValue = control.value as string;
    const originalControlValue = originalControl.value as string;

    if (controlValue !== originalControlValue) {
      return {
        repeatedPassword: {
          value: controlValue
        }
      };
    }

    return null;
  };
}
