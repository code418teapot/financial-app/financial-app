import { ChangeDetectionStrategy, Component } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {CreateProfileStore} from "./store";
import {controlValuesMatchValidator} from "./password.validators";

@Component({
  selector: 'app-create-profile',
  templateUrl: './create-profile.component.html',
  styleUrls: ['./create-profile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateProfileComponent {
  form: FormGroup<{ passportNumber: FormControl<string>; lastName: FormControl<string>; firstName: FormControl<string>; password: FormControl<string>; phone: FormControl<string>; confirmPassword: FormControl<string>; middleName: FormControl<string | null>; passportSeries: FormControl<string>; email: FormControl<string> }> = new FormGroup({
    lastName: new FormControl('', {
      nonNullable: true,
      validators: [Validators.required]
    }),
    firstName: new FormControl('', {
      nonNullable: true,
      validators: [Validators.required]
    }),
    middleName: new FormControl('', { nonNullable: false }),
    email: new FormControl('', {
      nonNullable: true,
      validators: [
        Validators.required,
        Validators.email
      ]
    }),
    phone: new FormControl('', {
      nonNullable: true,
      validators: [
        Validators.required,
        Validators.pattern(/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/)
      ]
    }),
    passportSeries: new FormControl('', {
      nonNullable: true,
      validators: [Validators.required, Validators.pattern(/^\d{4}$/)]
    }),
    passportNumber: new FormControl('', {
      nonNullable: true,
      validators: [Validators.required, Validators.pattern(/^\d{6}$/)]
    }),
    password: new FormControl('', {
      nonNullable: true,
      validators: [Validators.required]
    }),
    confirmPassword: new FormControl('', {
      nonNullable: true,
      validators: [
        Validators.required,
        controlValuesMatchValidator(() => this.form?.controls?.password),
      ]
    }),
  });

  constructor(
    private _formBuilder: FormBuilder,
    private _store: CreateProfileStore
  ) { }

  handleFormSubmit() {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }

    this._store.createProfile(this.form.getRawValue());
  }
}
