import {ComponentStore} from "@ngrx/component-store";
import {AuthenticationDataAccessService} from "../../../../data-access";
import {CreateProfileFormValue} from "./types";
import {catchError, EMPTY, finalize, Observable, switchMap, tap} from "rxjs";
import {Injectable} from "@angular/core";

interface CreateProfileStoreState {
  isLoading: boolean;
}

@Injectable()
export class CreateProfileStore extends ComponentStore<CreateProfileStoreState> {
  isLoading$ = this.select((state) => state.isLoading);

  constructor(private _authenticationDataAccessService: AuthenticationDataAccessService) {
    super({
      isLoading: false
    });
  }

  readonly createProfile = this.effect((createProfile$: Observable<CreateProfileFormValue>) => {
    return createProfile$.pipe(
      tap(() => this.patchState({ isLoading: true })),
      switchMap((form) => this._authenticationDataAccessService.createProfile({
        lastName: form.lastName,
        firstName: form.firstName,
        middleName: form.middleName,
        email: form.email,
        phoneNumber: form.phone,
        passport: `${form.passportSeries}${form.passportNumber}`,
        password: form.password
      }).pipe(
        finalize(() => this.patchState({ isLoading: false })),
        tap({
          next: () => alert("Created successfully"),
          error: (e) => console.log(e),
        }),
        catchError(() => EMPTY),
      )),
    );
  });
}
