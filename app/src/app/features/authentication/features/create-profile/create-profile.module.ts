import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateProfileComponent } from './create-profile.component';
import { RouterModule, Routes } from "@angular/router";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";
import { ReactiveFormsModule } from "@angular/forms";
import { DataAccessModule } from "../../../../data-access";
import { CreateProfileStore } from "./store";

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: CreateProfileComponent,
  }
]
@NgModule({
  declarations: [
    CreateProfileComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    ReactiveFormsModule,
    DataAccessModule
  ],
  providers: [
    CreateProfileStore
  ]
})
export class CreateProfileModule { }
