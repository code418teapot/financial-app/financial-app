import {ChangeDetectionStrategy, Component, SecurityContext} from '@angular/core';
import {DomSanitizer} from "@angular/platform-browser";
import {ApplicationRoutes} from "../../../../core/routing";
import {AuthenticationRoutes} from "../../routing/routes.enum";

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WelcomeComponent {
  imageSrc = this._domSanitizer.sanitize(SecurityContext.URL, "/assets/images/welcome.png");

  signInLink = ['/', ApplicationRoutes.Authentication, AuthenticationRoutes.SignIn];
  createProfileLink = ['/', ApplicationRoutes.Authentication, AuthenticationRoutes.CreateProfile];

  constructor(private _domSanitizer: DomSanitizer) { }
}
