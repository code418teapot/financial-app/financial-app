import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignInComponent } from './sign-in.component';
import { RouterModule, Routes } from "@angular/router";
import { ReactiveFormsModule } from "@angular/forms";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatButtonModule } from "@angular/material/button";
import { DataAccessModule } from "../../../../data-access";
import { SignInStore } from "./store";
import {PageModule, VariableModule} from "../../../../shared";

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: SignInComponent,
  }
]

@NgModule({
  declarations: [
    SignInComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    DataAccessModule,
    VariableModule,
    PageModule
  ],
  providers: [
    SignInStore
  ]
})
export class SignInModule { }
