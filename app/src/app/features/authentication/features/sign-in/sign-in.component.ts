import { ChangeDetectionStrategy, Component } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {SignInStore} from "./store";

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SignInComponent {
  form = new FormGroup({
    login: new FormControl('', {
      nonNullable: true,
      validators: [Validators.required]
    }),
    password: new FormControl('', {
      nonNullable: true,
      validators: [Validators.required]
    }),
  });

  readonly isLoading$ = this._store.isLoading$;
  readonly hasErrors$ = this._store.hasErrors$;

  constructor(private _store: SignInStore) {
  }

  handleFormSubmit() {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }

    this._store.signIn(this.form.getRawValue())
  }

  handleBackButtonClicked() {
    this._store.goBack();
  }
}
