export interface SignInFormReturnValue {
  login: string;
  password: string;
}
