export const featureName = 'authentication';

export interface AuthTokens {
  accessToken: string;
  refreshToken: string;
  expiresIn: number;
}

export interface SignInActionPropsValue {
  email: string;
  password: string;
}

export interface LoadingState {
  isLoading: boolean;
}

export interface NotAuthenticatedState {
  isAuthenticated: false;
}

export interface AuthenticatedState {
  isAuthenticated: true;
  tokens: AuthTokens;
  isAdmin: boolean;
}

export type AuthenticationState = (NotAuthenticatedState | AuthenticatedState) &
  LoadingState;
