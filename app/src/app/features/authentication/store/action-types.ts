export enum AuthenticationActionTypes {
  SignIn = '[Authentication] Sign In',
  SignInSuccess = '[Authentication] Sign In Success',
  SignInFailure = '[Authentication] Sign In Failure',
  SignInRequired = '[Authentication] Sign In Required',
  SignOut = '[Authentication] Sign Out',
}
