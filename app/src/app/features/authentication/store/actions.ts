import { createAction, props } from '@ngrx/store';
import { AuthenticationActionTypes } from './action-types';
import { AuthTokens, SignInActionPropsValue } from './types';

export const signIn = createAction(
  AuthenticationActionTypes.SignIn,
  props<SignInActionPropsValue>()
);

export const signInSuccess = createAction(
  AuthenticationActionTypes.SignInSuccess,
  props<AuthTokens>()
);

export const signInFailure = createAction(
  AuthenticationActionTypes.SignInFailure
);

export const signOut = createAction(AuthenticationActionTypes.SignOut);

export const signInRequired = createAction(
  AuthenticationActionTypes.SignInRequired
);
