import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';

import { of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import {
  signIn,
  signInFailure,
  signInRequired,
  signInSuccess,
} from './actions';
import {ApplicationRoutes} from "../../../core/routing";
import {AuthenticationRoutes} from "../routing/routes.enum";
import {AuthenticationDataAccessService} from "../../../data-access";

@Injectable()
export class AuthenticationEffects {
  signIn$ = createEffect(() =>
    this._actions$.pipe(
      ofType(signIn),
      switchMap((value) =>
        this._authenticationDataAccessService.signIn(value).pipe(
          map((tokens) => signInSuccess(tokens)),
          catchError(() => of(signInFailure()))
        )
      )
    )
  );

  navigateToSignInRoute$ = createEffect(
    () =>
      this._actions$.pipe(
        ofType(signInRequired),
        tap(() => {
          this._router.navigate([
            '/',
            ApplicationRoutes.Authentication,
            AuthenticationRoutes.SignIn,
          ]);
        })
      ),
    { dispatch: false }
  );

  constructor(
    private _globalStore: Store,
    private _actions$: Actions,
    private _router: Router,
    private _authenticationDataAccessService: AuthenticationDataAccessService
  ) {}
}
