import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { AuthenticationEffects } from './effects';
import { featureName } from './types';
import { authenticationReducer } from './reducers';
import {DataAccessModule} from "../../../data-access";

@NgModule({
  imports: [
    StoreModule.forFeature(featureName, authenticationReducer),
    EffectsModule.forFeature([AuthenticationEffects]),
    DataAccessModule
  ],
  providers: [],
})
export class AuthenticationStoreModule {}
