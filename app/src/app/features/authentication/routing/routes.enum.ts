export enum AuthenticationRoutes {
  Welcome= 'welcome',
  CreateProfile = 'create-profile',
  SignIn = 'sign-in',
  AccountVerification = 'account-verification'
}
