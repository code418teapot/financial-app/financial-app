import { Routes } from "@angular/router";
import { AuthenticationComponent } from "../authentication.component";
import {AuthenticationRoutes} from "./routes.enum";

export const authenticationRoutes: Routes = [
  {
    path: '',
    component: AuthenticationComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: AuthenticationRoutes.Welcome,
      },
      {
        path: AuthenticationRoutes.Welcome,
        loadChildren: async () =>
          (await import('../features/welcome')).WelcomeModule,
      },
      {
        path: AuthenticationRoutes.CreateProfile,
        loadChildren: async () =>
          (await import('../features/create-profile')).CreateProfileModule,
      },
      {
        path: AuthenticationRoutes.SignIn,
        loadChildren: async () =>
          (await import('../features/sign-in')).SignInModule,
      },
    ],
  },
];
