import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AuthenticationGuard} from "./guards/authentication.guard";
import {AuthenticationStoreModule} from "../store/store.module";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {TokenInterceptor} from "./interceptors/token.interceptor";
import {AdminGuard} from "./guards/admin.guard";

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    AuthenticationGuard,
    AdminGuard,
  ],
  exports: [
    AuthenticationStoreModule
  ]
})
export class AuthenticationCoreModule { }
