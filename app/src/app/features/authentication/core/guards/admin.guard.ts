import {Injectable} from "@angular/core";
import {CanActivate} from "@angular/router";
import {select, Store} from "@ngrx/store";
import {map, Observable} from "rxjs";
import {signInRequired} from "../../store/actions";
import {authSelector} from "../../store/selectors";

@Injectable()
export class AdminGuard implements CanActivate {
  constructor(private _globalStore: Store) {}

  canActivate(): Observable<boolean> {
    return this._globalStore.pipe(
      select(authSelector),
      map((authState) => {
        if (!authState.isAuthenticated || !authState.isAdmin) {
          this._globalStore.dispatch(signInRequired());

          console.log('Admin - guard - false');
          return false;
        }

        console.log('Admin - guard - true');
        return true;
      })
    );
  }
}
