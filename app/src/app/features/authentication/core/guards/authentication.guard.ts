import {Injectable} from "@angular/core";
import {CanActivate} from "@angular/router";
import {select, Store} from "@ngrx/store";
import {map, Observable} from "rxjs";
import {signInRequired} from "../../store/actions";
import {isAuthenticatedSelector} from "../../store/selectors";

@Injectable()
export class AuthenticationGuard implements CanActivate {
  constructor(private _globalStore: Store) {}

  canActivate(): Observable<boolean> {
    return this._globalStore.pipe(
      select(isAuthenticatedSelector),
      map((isAuthenticated) => {
        if (!isAuthenticated) {
          this._globalStore.dispatch(signInRequired());

          return false;
        }

        return true;
      })
    );
  }
}
