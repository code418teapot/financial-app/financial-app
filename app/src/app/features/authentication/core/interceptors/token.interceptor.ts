import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {first, Observable, switchMap} from 'rxjs';
import { Store } from '@ngrx/store';
import { AuthenticatedState } from "../../store/types";
import { authSelector } from "../../store/selectors";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(
    private _globalStore: Store,
  ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this._proxyRequestWithAuthToken(request, next);
  }

  private _selectAuth(): Observable<AuthenticatedState> {
    return this._globalStore
      .select(authSelector)
      .pipe(first()) as Observable<AuthenticatedState>;
  }

  private _proxyRequestWithAuthToken(
    request: HttpRequest<unknown>,
    next: HttpHandler,
  ): Observable<HttpEvent<unknown>> {
    return this._selectAuth().pipe(
      switchMap((auth) => {
        if (!auth.isAuthenticated) {
          return next.handle(request);
        }

        const { accessToken } = auth.tokens;

        const authenticatedRequest = accessToken
          ? request.clone({
            setHeaders: { Authorization: `Bearer ${accessToken}` },
          })
          : request;

        return next.handle(authenticatedRequest);
      }),
    );
  }
}
