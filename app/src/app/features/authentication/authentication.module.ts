import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthenticationComponent } from './authentication.component';
import {RouterModule} from "@angular/router";
import {authenticationRoutes} from "./routing";

@NgModule({
  declarations: [
    AuthenticationComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(authenticationRoutes)
  ]
})
export class AuthenticationModule { }
