import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DocsComponent } from './docs.component';
import { RouterModule, Routes } from "@angular/router";
import {PageModule} from "../../shared";

const routes: Routes = [
  {
    path: ':docId',
    component: DocsComponent
  }
]

@NgModule({
  declarations: [
    DocsComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    PageModule,
  ]
})
export class DocsModule { }
