import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {HttpClient,} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";
import {filter, map, switchMap} from "rxjs";
import {isNil} from "lodash";

const docsMap : {
  [doc: string]: string | undefined,
} = {
  ["terms"]: "terms_of_service.txt",
  ["privacy"]: "privacy_policy.txt",
}

const docsTitleMap : {
  [doc: string]: string | undefined,
} = {
  ["terms"]: "Условия использования",
  ["privacy"]: "Политика приватности",
}

@Component({
  selector: 'app-docs',
  templateUrl: './docs.component.html',
  styleUrls: ['./docs.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DocsComponent implements OnInit {
  title = "Загрузка...";

  content = "";

  constructor(
    private _httpClient: HttpClient,
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _changeDetectorRef: ChangeDetectorRef
  ) {
  }
  ngOnInit() {
    this._activatedRoute.params.pipe(
      map(params => params['docId'] as string ?? ''),
      map(docId => docsMap[docId]),
      filter(docUrl => !isNil(docUrl)),
      switchMap(docUrl => this._httpClient.get(`/assets/docs/${docUrl}`, {
        responseType: "text"
      }))
    )
      .subscribe(content => {
        this.content = content;
        this._changeDetectorRef.markForCheck()
      });

    this._activatedRoute.params.pipe(
      map(params => params['docId'] as string ?? ''),
      map(docId => docsTitleMap[docId]),
      filter(title => !isNil(title)),
    )
      .subscribe(title => {
        this.title = title ?? '';
        this._changeDetectorRef.markForCheck()
      })
  }
}
