import { NgModule } from '@angular/core';
import {MetaReducer, StoreModule} from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { saveStateInStorage } from './meta-reducers';

export const metaReducers: MetaReducer<any>[] = [saveStateInStorage(localStorage)];

@NgModule({
  imports: [
    StoreModule.forRoot(
      {},
      { metaReducers }
    ),

    // Goes afterwards because AppEffects has an access to state props but reducers should be
    // initialized first
    EffectsModule.forRoot([]),
  ],
  providers: [],
})
export class NgrxModule {}
