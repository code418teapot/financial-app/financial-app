export enum ApplicationRoutes {
  Authentication = 'authentication',
  NotFound = 'not-found',
  UnderDevelopment = 'coming-soon',
  Home = 'home',
  Settings = 'settings',
  Account = 'account',
  Admin = 'admin',
  Docs = 'docs'
}
