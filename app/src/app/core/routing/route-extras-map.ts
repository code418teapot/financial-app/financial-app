import {ApplicationRoutes} from "./routes.enum";
import {StatusPageExtras} from "../../shared";

export const statusPageExtras: {
  [path: string]: StatusPageExtras;
} = {
  [ApplicationRoutes.NotFound]: {
    title: '404',
    subTitle: 'Мы ничего не нашли для вас (',
    imageSrc: '/assets/images/oops.png'
  },
  [ApplicationRoutes.UnderDevelopment]: {
    title: 'OOPS',
    subTitle: 'Кажется мы не успели доделать эту страничку...Приходите позже!',
    imageSrc: '/assets/images/oops.png'
  }
};
