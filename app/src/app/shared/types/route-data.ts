export interface RouteData {
  animation?: {
    path: string;
  };
  extra?: StatusPageExtras;
}

export interface StatusPageExtras {
  title: string;
  subTitle: string;
  imageSrc?: string;
}
