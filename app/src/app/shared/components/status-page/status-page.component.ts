import { ChangeDetectionStrategy, Component, SecurityContext } from '@angular/core';
import { filter, map } from 'rxjs';
import { isNil } from 'lodash';
import { ActivatedRoute } from "@angular/router";
import { DomSanitizer } from "@angular/platform-browser";
@Component({
  selector: 'app-status-page',
  templateUrl: './status-page.component.html',
  styleUrls: ['./status-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StatusPageComponent {

  title = this._activatedRoute.data.pipe(
    filter((data) => !isNil(data?.['extra'])),
    map(({ extra }) => extra.title ?? 'Упс...'),
  );

  subTitle = this._activatedRoute.data.pipe(
    filter((data) => !isNil(data?.['extra'])),
    map(({ extra }) => extra.subTitle ?? 'Я чайник'),
  );

  image = this._activatedRoute.data.pipe(
    filter((data) => !isNil(data?.['extra'])),
    map(({ extra }) => this._domSanitizer.sanitize(
      SecurityContext.URL,
      extra.imageSrc ?? null
    ))
  );

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _domSanitizer: DomSanitizer
  ) {}
}
