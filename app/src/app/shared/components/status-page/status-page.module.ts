import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatusPageComponent } from './status-page.component';
import { RouterModule } from "@angular/router";

@NgModule({
  declarations: [
    StatusPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class StatusPageModule { }
