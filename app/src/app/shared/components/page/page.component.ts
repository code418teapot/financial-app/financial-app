import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PageComponent {
  @Input()
  title: string = '';

  @Input()
  showBackButton: boolean = false;

  @Output()
  backButtonClicked = new EventEmitter<void>();
}
