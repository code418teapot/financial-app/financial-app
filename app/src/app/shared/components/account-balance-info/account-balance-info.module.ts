import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AccountBalanceInfoComponent} from "./account-balance-info.component";
import {AppCurrencyPipeModule} from "../../pipes/app-currency-pipe/app-currency-pipe.module";

@NgModule({
  declarations: [AccountBalanceInfoComponent],
  imports: [
    CommonModule,
    AppCurrencyPipeModule
  ],
  exports: [
    AccountBalanceInfoComponent
  ]
})
export class AccountBalanceInfoModule { }
