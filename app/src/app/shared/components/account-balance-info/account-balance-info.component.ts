import {ChangeDetectionStrategy, Component, Input } from '@angular/core';
import {AccountBasicInfo} from "../../../features/home/types";

@Component({
  selector: 'app-account-balance-info[account]',
  templateUrl: './account-balance-info.component.html',
  styleUrls: ['./account-balance-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AccountBalanceInfoComponent {
  @Input()
  account!: AccountBasicInfo;
}
