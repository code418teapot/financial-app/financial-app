import { InjectionToken } from '@angular/core';
import { Environment } from "../types";

export const environmentToken = new InjectionToken<Environment>('environment');
