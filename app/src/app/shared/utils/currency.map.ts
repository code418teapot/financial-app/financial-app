export const currencyTitlesNameMap: {
  [key: string]: string
} = {
  ['USD']: 'Доллары США',
  ['EUR']: 'Евро',
  ['RUB']: 'Рубли',
  ['CAD']: 'Канадские доллары',
  ['CNY']: 'Китайский юань',
  ['JPY']: 'Японские йены'
}

export const currencySymbolsNameMap: {
  [key: string]: string
} = {
  ['USD']: '$',
  ['EUR']: '€',
  ['RUB']: '₽',
  ['CAD']: 'С$',
  ['CNY']: '¥',
  ['JPY']: '¥'

}

export const currencyIconsMap: {
  [key: string]: string
} = {
  ['USD']: 'attach_money',
  ['EUR']: 'euro_symbol',
  ['RUB']: 'currency_ruble',
  ['CAD']: 'attach_money',
  ['CNY']: 'currency_yen',
  ['JPY']: 'currency_yen'
}
