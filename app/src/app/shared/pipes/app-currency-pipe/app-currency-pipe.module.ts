import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AppCurrencyPipe} from "./app-currency.pipe";
@NgModule({
  declarations: [AppCurrencyPipe],
  imports: [
    CommonModule
  ],
  exports: [
    AppCurrencyPipe
  ]
})
export class AppCurrencyPipeModule {
}
