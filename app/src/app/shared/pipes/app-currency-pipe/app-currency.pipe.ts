import {Pipe, PipeTransform} from "@angular/core";
import {currencySymbolsNameMap} from "../../utils";

@Pipe({name:'appCurrency'})
export class AppCurrencyPipe implements PipeTransform {
  constructor() { }
  transform(value: string | number | null | undefined, currency: string): string {
    try {
      let amount;
      if (value === null || value === undefined) {
        amount = 0;
      }
      else {
        amount = Number(value);
      }

      return `${this._formatMoney(amount)} ${currencySymbolsNameMap[currency]}`
    } catch (error) {
      return value + ' ' + currency;
    }
  }

  private _formatMoney(amount: number | string) {
    return Number(amount)
      .toFixed(2)
      .replace(/\d(?=(\d{3})+,)/g, '$&.');
  }
}
