import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ApplicationRoutes, statusPageExtras} from "./core/routing";
import {RouteData, StatusPageComponent, StatusPageExtras} from "./shared";
import {AuthenticationGuard} from "./features/authentication/core/guards/authentication.guard";
import {AdminGuard} from "./features/authentication/core/guards/admin.guard";

function getRouteData(
  path: ApplicationRoutes,
  extrasMap: {
    [path: string]: StatusPageExtras;
  } = statusPageExtras,
): RouteData {
  return {
    animation: { path },
    extra: extrasMap[path],
  } as RouteData;
}

const routes: Routes = [
  {
    path: ApplicationRoutes.NotFound,
    component: StatusPageComponent,
    data: getRouteData(ApplicationRoutes.NotFound, statusPageExtras)
  },
  {
    path: ApplicationRoutes.UnderDevelopment,
    component: StatusPageComponent,
    data: getRouteData(ApplicationRoutes.UnderDevelopment, statusPageExtras)
  },
  {
    path: ApplicationRoutes.Authentication,
    loadChildren: async () =>
      (await import('./features/authentication')).AuthenticationModule,
  },
  {
    path: ApplicationRoutes.Home,
    loadChildren: async () =>
      (await import('./features/home')).HomeModule,
    canActivate: [AuthenticationGuard]
  },
  {
    path: ApplicationRoutes.Admin,
    loadChildren: async () =>
      (await import('./features/admin')).AdminModule,
    canActivate: [AdminGuard]
  },
  {
    path: ApplicationRoutes.Account,
    loadChildren: async () =>
      (await import('./features/account')).AccountModule,
    canActivate: [AuthenticationGuard]
  },
  {
    path: ApplicationRoutes.Docs,
    loadChildren: async () =>
      (await import('./features/docs')).DocsModule,
  },
  {
    path: ApplicationRoutes.Settings,
    redirectTo: ApplicationRoutes.UnderDevelopment
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: ApplicationRoutes.Authentication,
  },
  {
    path: '**',
    redirectTo: ApplicationRoutes.NotFound
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
