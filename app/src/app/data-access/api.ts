import {Environment} from "../shared";

const publicPath = 'public';
const protectedPath = 'protected';
const privatePath = 'private';

const authPath = 'auth';
const usersPath = 'users';
const accountPath = 'accounts';
const unitsPath = 'units';
const ordersPath = 'orders';

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export function buildApi({ apiUrl }: Environment) {
  return {
    authentication: {
      createProfile: () => `${apiUrl}/${publicPath}/${usersPath}/register`,
      signIn: () => `${apiUrl}/${publicPath}/${authPath}/login`,
    },
    admin: {
      getUsers: () => `${apiUrl}/${privatePath}/${usersPath}`,
      getUserById: (id: number) => `${apiUrl}/${privatePath}/${usersPath}/${id}`,
      activateUser: (id: number) => `${apiUrl}/${privatePath}/${usersPath}/activate/${id}`,
    },
    accounts: {
      createAccount: () => `${apiUrl}/${protectedPath}/${accountPath}`,
      getAccounts: () => `${apiUrl}/${protectedPath}/${accountPath}/about`,
      getAccount: (id: number) => `${apiUrl}/${protectedPath}/${accountPath}/${id}`,
      deposit: () => `${apiUrl}/${protectedPath}/${accountPath}/deposit`,
      withdraw: () => `${apiUrl}/${protectedPath}/${accountPath}/writeOff`,
      history: () => `${apiUrl}/${protectedPath}/${ordersPath}`
    },
    units: {
      getUnits: () => `${apiUrl}/${protectedPath}/${unitsPath}/list`,
      getUnitData: (code: string) => `${apiUrl}/${publicPath}/${unitsPath}/${code}`,
      order: (operationType: 'sell' | 'buy') => `${apiUrl}/${protectedPath}/${unitsPath}/operations/${operationType}`,
    }
  };
}
