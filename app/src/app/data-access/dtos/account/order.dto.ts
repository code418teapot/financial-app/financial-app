export interface OrderRequestDto {
  currency: string,
  amount: number;
  accountSenderId: number;
  accountReceiverId: number;
}
