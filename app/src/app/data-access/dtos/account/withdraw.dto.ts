export interface WithdrawRequestDto {
  currency: "RUB",
  amount: number,
  accountSenderId: number;
}
