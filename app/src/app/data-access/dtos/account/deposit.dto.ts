export interface DepositRequestDto {
  currency: "RUB",
  amount: number,
  accountReceiverId: number;
}
