export interface OrderHistoryItemDto {
  type: 'BUY' | 'SELL' | 'DEPOSIT' | 'WITHDRAW',
  status: 'WAITING' | 'PROCESSING' | 'PROCESSED',
  currency: string;
  amount: number;
  exchangeRate: number;
  accountSenderId: number;
  accountRecevierId: number;

  createdDate: string;
}
