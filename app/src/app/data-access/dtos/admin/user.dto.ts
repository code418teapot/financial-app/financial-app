export interface UserDto {
  id: number,
  firstName: string,
  lastName: string,
  middleName?: string,
  email: string,
  phoneNumber: string,
  active: true,
  roles: string[]
}
