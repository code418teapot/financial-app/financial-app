export interface AccountBasicInfoDto {
  id: number;
  createdDate: string;
  updatedDate: string;
  currency: string;
  number: string;
  balance: number;
  rubBalance: number;
}
