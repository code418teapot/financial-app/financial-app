export interface QuoteDto {
  datetime: string,
  open: number,
  high: number,
  low: number,
  close: number
}
