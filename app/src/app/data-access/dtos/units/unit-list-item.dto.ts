export interface UnitListItemDto {
  type: string,
  code: string,
  fullName: string,
  symbol: string
}
