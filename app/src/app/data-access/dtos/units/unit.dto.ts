import {QuoteDto} from "./quote.dto";

export interface UnitDto {
  fullName: string;
  exchangeRate: number;
  minutelyQuoteData: QuoteDto[]
}
