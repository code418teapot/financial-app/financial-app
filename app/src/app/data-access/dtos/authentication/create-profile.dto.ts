export interface CreateProfileDto {
  lastName: string;
  firstName: string;
  middleName?: string | null;
  email: string;
  phoneNumber: string
  passport: string
  password: string
}
