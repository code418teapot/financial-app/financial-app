export interface TokensDto {
  accessToken: string;
  refreshToken: string;
  expiresIn: number;
}
