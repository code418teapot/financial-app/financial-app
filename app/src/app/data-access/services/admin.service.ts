import {Inject, Injectable} from "@angular/core";
import {buildApi} from "../api";
import {HttpClient} from "@angular/common/http";
import {Environment, environmentToken} from "../../shared";
import {Observable} from "rxjs";
import {UserDto} from "../dtos/admin/user.dto";

@Injectable()
export class AdminDataAccessService {
  private _api = buildApi(this._environment);

  constructor(
    private _httpClient: HttpClient,
    @Inject(environmentToken) private _environment: Environment
  ) {
  }

  getUsers(): Observable<UserDto[]> {
    return this._httpClient
      .get<UserDto[]>(this._api.admin.getUsers());
  }

  getUserById(id: number): Observable<UserDto> {
    return this._httpClient
      .get<UserDto>(this._api.admin.getUserById(id));
  }

  activateUser(id: number): Observable<unknown> {
    return this._httpClient
      .post<unknown>(this._api.admin.activateUser(id), undefined);
  }
}
