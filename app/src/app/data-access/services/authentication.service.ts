import {Inject, Injectable} from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { buildApi } from "../api";
import { Environment, environmentToken } from "../../shared";
import { CreateProfileDto } from "../dtos/authentication/create-profile.dto";
import { Observable } from "rxjs";
import {SignInRequestDto} from "../dtos/authentication/sign-in-request.dto";
import {TokensDto} from "../dtos/authentication/tokens.dto";

@Injectable()
export class AuthenticationDataAccessService {
  private _api = buildApi(this._environment);

  constructor(
    private _httpClient: HttpClient,
    @Inject(environmentToken) private _environment: Environment
  ) {}

  createProfile(createProfileDto: CreateProfileDto): Observable<unknown> {
    return this._httpClient
      .post<unknown>(this._api.authentication.createProfile(), createProfileDto);
  }

  signIn(signInRequestDto: SignInRequestDto): Observable<TokensDto> {
    return this._httpClient
      .post<TokensDto>(this._api.authentication.signIn(), signInRequestDto);
  }
}
