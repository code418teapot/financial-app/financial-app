import {Inject, Injectable} from "@angular/core";
import {buildApi} from "../api";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Environment, environmentToken} from "../../shared";
import {Observable} from "rxjs";
import {AccountBasicInfoDto} from "../dtos/home/account-basic-info.dto";
import {DepositRequestDto} from "../dtos/account/deposit.dto";
import {WithdrawRequestDto} from "../dtos/account/withdraw.dto";
import {OrderRequestDto} from "../dtos/account/order.dto";
import {OrderHistoryItemDto} from "../dtos/account/order-history-item.dto";

@Injectable()
export class AccountsDataAccessService {
  private _api = buildApi(this._environment);

  constructor(
    private _httpClient: HttpClient,
    @Inject(environmentToken) private _environment: Environment
  ) {}

  getAccounts(): Observable<AccountBasicInfoDto[]> {
    return this._httpClient
      .get<AccountBasicInfoDto[]>(this._api.accounts.getAccounts());
  }

  getAccount(accountId: number): Observable<AccountBasicInfoDto> {
    return this._httpClient
      .get<AccountBasicInfoDto>(this._api.accounts.getAccount(accountId));
  }

  deposit(depositRequestDto: DepositRequestDto): Observable<unknown> {
    return this._httpClient
      .post<unknown>(this._api.accounts.deposit(), depositRequestDto);
  }

  withdraw(withdrawRequestDto: WithdrawRequestDto): Observable<unknown> {
    return this._httpClient
      .post<unknown>(this._api.accounts.withdraw(), withdrawRequestDto);
  }

  order(orderRequestDto: OrderRequestDto, operationType: 'sell' | 'buy'): Observable<unknown> {
    return this._httpClient
      .post<unknown>(this._api.units.order(operationType), orderRequestDto);
  }

  getHistory(): Observable<OrderHistoryItemDto[]> {
    return this._httpClient
      .get<OrderHistoryItemDto[]>(this._api.accounts.history());
  }

  createAccount(currency: string): Observable<unknown> {
    const params = new HttpParams().set('currency', currency ?? '');

    return this._httpClient.post<unknown[]>(
      this._api.accounts.createAccount(),
      undefined,
      {
        params,
      }
    );
  }
}
