import {Inject, Injectable} from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { buildApi } from "../api";
import { Environment, environmentToken } from "../../shared";
import {Observable} from "rxjs";
import {UnitDto} from "../dtos/units/unit.dto";
import {UnitListItemDto} from "../dtos/units/unit-list-item.dto";

@Injectable()
export class UnitsDataAccessService {
  private _api = buildApi(this._environment);

  constructor(
    private _httpClient: HttpClient,
    @Inject(environmentToken) private _environment: Environment
  ) {
  }

  getUnits(): Observable<UnitListItemDto[]> {
    return this._httpClient
      .get<UnitListItemDto[]>(this._api.units.getUnits());
  }

  getUnitsInformation(code: string): Observable<UnitDto> {
    return this._httpClient
      .get<UnitDto>(this._api.units.getUnitData(code));
  }
}
