import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  AccountsDataAccessService,
  AdminDataAccessService,
  AuthenticationDataAccessService,
  UnitsDataAccessService
} from "./services";

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
  ],
  providers: [
    AuthenticationDataAccessService,
    AccountsDataAccessService,
    UnitsDataAccessService,
    AdminDataAccessService,
  ]
})
export class DataAccessModule { }
