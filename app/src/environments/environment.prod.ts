import {Environment} from "../app/shared";

export const environment: Environment = {
  production: true,
  apiUrl: 'http://api.financeapp.site'
};
